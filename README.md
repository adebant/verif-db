# Symbolic verification of distance-bounding protocols

In this repository we provide the full material that have been used to conduct the case studies presented in the following manuscript: [manuscript.pdf](https://gitlab.inria.fr/adebant/verif-db/-/blob/master/manuscrit.pdf)

We distinguish three sub-repositories:
- akiss-db: it contains the source code related to the new procedure which extends the [Akiss tool](http://akiss.gforge.inria.fr), together with the corresponding case studies;
- reduction-results: it contains the material related to the analyses that have been conducted thanks to the reduction results;
- EMV-payment: it contains the material that have been used to prove the security of the two novel EMV-payment protocols PayBCR and PayCCR.

More details about the use of the frameworks are provided in each sub-repository.