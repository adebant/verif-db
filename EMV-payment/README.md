# Analyses of two novel EMV-payment protocols: PayBCR and PayCCR

This folder contains the material corresponding to the analyses of the two protocols PayBCR and PayCCR presented in Chapter 7. 

The analyses have benn performed relying on the [ProVerif tool](https://prosecco.gforge.inria.fr/personal/bblanche/proverif/). 
They have been conducted using ProVerif 2.00 executed on a standard laptop. 

All the files for the different protocols  are available on the following drive: drive-examples. 
There are three files: 
- payBCR.pv: models the PayBCR protocol as described in Figure 1 in the submitted paper;
- payBCR-.pv: models the PayBCR protocol with indistinguishable certificates for cards and TPMs;
- payCCR++.pv: models the PayCCR++ protocol which is a slightly modified version of the PayCCR protocol in which the TPM identity and the two timestamps are added in the AC message (as described in Section 4.3).
 
| Protocol | Role authetication | Time-bound authentication | Causality-based security |
| -------- | :----------------: | :-----------------------: | :----------------------: |
| PayCCR++ | ❌                 | ✅                        |     ✅                  |   
| PayBCR-  | ❌                 | ✅                        |     ✅                  |   
| PayBCR   | ✅                 | ✅                        |     ✅                  |   


Details are also available in the corresponding publication: 
- Ioana Boureanu, Tom Chothia, Alexandre Debant, and Stéphanie Delaune. *Security Analysis and Implementation of Relay-Resistant Contactless Payments*.<p> 
 (To appear in) Proceedings of the 2020 ACM SIGSAC Conference on Computer and Communications Security. ACM, 2020