(* ----------------------------------------- *)
(*          MEADOWS (XOR1) PROTOCOL          *)
(*       F(nV,nP,P) = (XOR(nV,nP),idP)       *)
(*     Distance Hijacking Fraud Analysis     *)
(* ----------------------------------------- *)

(* -- Protocol Scheme -- *)
(* P,V share a key K for MAC purposes.
V -> P : nV
P -> V : Answer(nV,nP,idP) = < XOR(nV,nP), idP >
P -> V : <idP,nP,nV> , MAC(K,<idP,nP,nV>)
*)

(* -- Signature --- *)
const OK:bitstring.

(* MAC *)
fun MAC(bitstring,bitstring):bitstring.

(* Shared key *)
fun sharedKey(bitstring,bitstring):bitstring [private].

(* Weak XOR *)
fun XOR(bitstring,bitstring):bitstring.
reduc forall x:bitstring, y:bitstring; commutXOR(XOR(x,y)) = XOR(y,x).
reduc forall x:bitstring, y:bitstring; cancelXOR1(XOR(x,y),x) = y.
reduc forall x:bitstring, y:bitstring; cancelXOR2(XOR(x,y),y) = x.
(*
reduc forall x:bitstring, y:bitstring, z:bitstring; cancelTXOR1(y,XOR(x,XOR(y,z))) = XOR(x,z).
reduc forall x:bitstring, y:bitstring, z:bitstring; cancelTXOR2(z,XOR(x,XOR(y,z))) = XOR(x,y).
*)

(* Equality Check Function *)
fun equals(bitstring,bitstring):bitstring
reduc forall x:bitstring; equals(x,x) = OK.

(* Pair function *)
(* NOTE : We need a special pairing function for our analysis of distance hijacking fraud using our modified proverif executable.*)
fun Pair(bitstring,bitstring):bitstring.
fun Proj1(bitstring):bitstring
reduc forall x:bitstring, y:bitstring; Proj1(Pair(x,y)) = x.
fun Proj2(bitstring):bitstring
reduc forall x:bitstring, y:bitstring; Proj2(Pair(x,y)) = y.

(* --- Constants / Channels --- *)
free cP:channel.
free idV0:bitstring. (* Honest Player acting as Test Verifier *)
free idP0:bitstring. (* Dishonest Player far-away from idV0 *)
free idE0:bitstring. (* Honest Player far-away from idV0 *)

(* --- Events --- *)
event EndV(bitstring,bitstring).

(* --- Processes --- *)
let Verifier_Test(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	phase 1;
	out(cP,nV);
	in(cP,inV1:bitstring);
	phase 2;	
	let testID = equals(ipID,Proj2(inV1)) in
	let ipN = cancelXOR1(Proj1(inV1),nV) in
	in(cP,inV2:bitstring);
	let testPair = equals(Pair(ipID,Pair(ipN,nV)),Proj1(inV2)) in
	let TestMAC = equals(MAC(Pair(ipID,Pair(ipN,nV)),sharedKey(vID,ipID)),Proj2(inV2)) in
	event EndV(vID,ipID).	
	
let Verifier_P0(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);
	let testID = equals(ipID,Proj2(inV1)) in
	let ipN = cancelXOR1(Proj1(inV1),nV) in
	in(cP,inV2:bitstring);
	let testPair = equals(Pair(ipID,Pair(ipN,nV)),Proj1(inV2)) in
	let TestMAC = equals(MAC(Pair(ipID,Pair(ipN,nV)),sharedKey(vID,ipID)),Proj2(inV2)) in
	0.

let Verifier_P1(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	phase 1;	
	in(cP,inV1:bitstring);
	let testID = equals(ipID,Proj2(inV1)) in
	let ipN = cancelXOR1(Proj1(inV1),nV) in
	in(cP,inV2:bitstring);
	let testPair = equals(Pair(ipID,Pair(ipN,nV)),Proj1(inV2)) in
	let TestMAC = equals(MAC(Pair(ipID,Pair(ipN,nV)),sharedKey(vID,ipID)),Proj2(inV2)) in
	0.
	
let Verifier_P2(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	phase 2;	
	in(cP,inV1:bitstring);
	let testID = equals(ipID,Proj2(inV1)) in
	let ipN = cancelXOR1(Proj1(inV1),nV) in
	in(cP,inV2:bitstring);
	let testPair = equals(Pair(ipID,Pair(ipN,nV)),Proj1(inV2)) in
	let TestMAC = equals(MAC(Pair(ipID,Pair(ipN,nV)),sharedKey(vID,ipID)),Proj2(inV2)) in
	0.
	
let Verifier_P0P1(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);
	let testID = equals(ipID,Proj2(inV1)) in
	let ipN = cancelXOR1(Proj1(inV1),nV) in
	phase 1;
	in(cP,inV2:bitstring);
	let testPair = equals(Pair(ipID,Pair(ipN,nV)),Proj1(inV2)) in
	let TestMAC = equals(MAC(Pair(ipID,Pair(ipN,nV)),sharedKey(vID,ipID)),Proj2(inV2)) in
	0.
	
let Verifier_P0P2(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);
	let testID = equals(ipID,Proj2(inV1)) in
	let ipN = cancelXOR1(Proj1(inV1),nV) in
	phase 2;
	in(cP,inV2:bitstring);
	let testPair = equals(Pair(ipID,Pair(ipN,nV)),Proj1(inV2)) in
	let TestMAC = equals(MAC(Pair(ipID,Pair(ipN,nV)),sharedKey(vID,ipID)),Proj2(inV2)) in
	0.

let Verifier_P1P2(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	phase 1;	
	in(cP,inV1:bitstring);
	let testID = equals(ipID,Proj2(inV1)) in
	let ipN = cancelXOR1(Proj1(inV1),nV) in
	phase 2;
	in(cP,inV2:bitstring);
	let testPair = equals(Pair(ipID,Pair(ipN,nV)),Proj1(inV2)) in
	let TestMAC = equals(MAC(Pair(ipID,Pair(ipN,nV)),sharedKey(vID,ipID)),Proj2(inV2)) in
	0.
	
let Prover_P0(pID:bitstring,ivID:bitstring) =
	in(cP,inP1:bitstring);
	new nP:bitstring;
	let Ans = Pair(XOR(inP1,nP),pID) in
	out(cP,Ans);
	let pMAC = MAC(Pair(pID,Pair(nP,inP1)),sharedKey(ivID,pID)) in
	let Conf = Pair(Pair(pID,Pair(nP,inP1)),pMAC) in
	out(cP,Conf).
	
let Prover_P1(pID:bitstring,ivID:bitstring) =
	phase 1;
	in(cP,inP1:bitstring);
	new nP:bitstring;
	let Ans = Pair(XOR(inP1,nP),pID) in
	out(cP,Ans);
	let pMAC = MAC(Pair(pID,Pair(nP,inP1)),sharedKey(ivID,pID)) in
	let Conf = Pair(Pair(pID,Pair(nP,inP1)),pMAC) in
	out(cP,Conf).

let Prover_P2(pID:bitstring,ivID:bitstring) =
	phase 2;
	in(cP,inP1:bitstring);
	new nP:bitstring;
	let Ans = Pair(XOR(inP1,nP),pID) in
	out(cP,Ans);
	let pMAC = MAC(Pair(pID,Pair(nP,inP1)),sharedKey(ivID,pID)) in
	let Conf = Pair(Pair(pID,Pair(nP,inP1)),pMAC) in
	out(cP,Conf).
	
(* --- Security Property --- *)
query event(EndV(idV0,idP0)).

(* --- Global Process --- *)
process
	(* Secrets known to the adversary *)
	out(cP,sharedKey(idV0,idP0));	
	out(cP,sharedKey(idP0,idV0));
	out(cP,sharedKey(idE0,idP0));
	out(cP,sharedKey(idP0,idE0));
	out(cP,sharedKey(idP0,idP0));		
	(* Now we let the adversary (almost) alone *)
	(* Honest Verifier used to test the DHR property *)
	Verifier_Test(idV0,idP0)|
	(* Processes that can also speak during the execution : *)
	(* idV0 may execute Prover/Verifier roles at anytime *)
	!Verifier_P0(idV0,idV0) | !Verifier_P1(idV0,idV0) | !Verifier_P2(idV0,idV0) |
	!Verifier_P0P1(idV0,idV0) | !Verifier_P0P2(idV0,idV0) | !Verifier_P1P2(idV0,idV0) |	
	!Verifier_P0(idV0,idP0) | !Verifier_P1(idV0,idP0) | !Verifier_P2(idV0,idP0) |
	!Verifier_P0P1(idV0,idP0) | !Verifier_P0P2(idV0,idP0) | !Verifier_P1P2(idV0,idP0) |	
	!Verifier_P0(idV0,idE0) | !Verifier_P1(idV0,idE0) | !Verifier_P2(idV0,idE0) |
	!Verifier_P0P1(idV0,idE0) | !Verifier_P0P2(idV0,idE0) | !Verifier_P1P2(idV0,idE0) |		
	!Prover_P0(idV0,idV0) | !Prover_P1(idV0,idV0) | !Prover_P2(idV0,idV0) |	
	!Prover_P0(idV0,idP0) | !Prover_P1(idV0,idP0) | !Prover_P2(idV0,idP0) |	
	!Prover_P0(idV0,idE0) | !Prover_P1(idV0,idE0) | !Prover_P2(idV0,idE0) |
	(* idE0 only speaks in P0/P2 *)
	!Verifier_P0(idE0,idV0) | !Verifier_P2(idE0,idV0) | !Verifier_P0P2(idE0,idV0) |
	!Verifier_P0(idE0,idP0) | !Verifier_P2(idE0,idP0) | !Verifier_P0P2(idE0,idP0) |
	!Verifier_P0(idE0,idE0) | !Verifier_P2(idE0,idE0) | !Verifier_P0P2(idE0,idE0) |	
	!Prover_P0(idE0,idV0) | !Prover_P2(idE0,idV0) |
	!Prover_P0(idE0,idP0) | !Prover_P2(idE0,idP0) |
	!Prover_P0(idE0,idE0) | !Prover_P2(idE0,idE0)