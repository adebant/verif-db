(* ----------------------------------------- *)
(*            SPADE Fixed PROTOCOL           *)
(*     Distance Hijacking Fraud Analysis     *)
(* ----------------------------------------- *)

(* -- Protocol Scheme -- *)
(* P,V have secret/public keys skP/pkP, skV/pkV.
P -> V : Encrypt(<nP,idV,Sign(nP,skP)>,pkV)
V -> P : mV,nV
V -> P : cV
P -> V : Answer(cV,PRF(nP,nV),XOR(R0,nP,mV))
P -> V : PRF(nP,nV,mV,cV,rP)
*)

(* -- Signature --- *)
const OK:bitstring.

(* Public-key encryption *)
fun sKey(bitstring):bitstring [private].
fun pKey(bitstring):bitstring.

fun Encrypt(bitstring,bitstring):bitstring.
fun Decrypt(bitstring,bitstring):bitstring
reduc forall x:bitstring, y:bitstring; Decrypt(Encrypt(x,pKey(y)),sKey(y)) = x.

(* Signature *)
fun ssKey(bitstring):bitstring [private].
fun spKey(bitstring):bitstring.

fun Sign(bitstring,bitstring):bitstring.
fun Verify(bitstring,bitstring):bitstring
reduc forall x:bitstring, y:bitstring; Verify(Sign(x,ssKey(y)),spKey(y)) = x.

(* PRF functions *)
fun PRF(bitstring):bitstring.

(* Weak XOR *)
fun XOR(bitstring,bitstring):bitstring.
reduc forall x:bitstring, y:bitstring; commutXOR(XOR(x,y)) = XOR(y,x).
reduc forall x:bitstring, y:bitstring; cancelXOR1(XOR(x,y),x) = y.
reduc forall x:bitstring, y:bitstring; cancelXOR2(XOR(x,y),y) = x.
reduc forall x:bitstring, y:bitstring, z:bitstring; cancelTXOR1(y,XOR(x,XOR(y,z))) = XOR(x,z).
reduc forall x:bitstring, y:bitstring, z:bitstring; cancelTXOR2(z,XOR(x,XOR(y,z))) = XOR(x,y).

(* Equality Check Function *)
fun equals(bitstring,bitstring):bitstring
reduc forall x:bitstring; equals(x,x) = OK.

(* Answering function *)
fun Answer(bitstring,bitstring,bitstring):bitstring.

(* Pair function *)
(* NOTE : We need a special pairing function for our analysis of distance hijacking fraud using our modified proverif executable.*)
fun Pair(bitstring,bitstring):bitstring.
fun Proj1(bitstring):bitstring
reduc forall x:bitstring, y:bitstring; Proj1(Pair(x,y)) = x.
fun Proj2(bitstring):bitstring
reduc forall x:bitstring, y:bitstring; Proj2(Pair(x,y)) = y.

(* --- Constants / Channels --- *)
free cP:channel.
free idV0:bitstring. (* Honest Player acting as Test Verifier *)
free idP0:bitstring. (* Dishonest Player far-away from idV0 *)
free idE0:bitstring. (* Honest Player far-away from idV0 *)

(* --- Events --- *)
event EndV(bitstring,bitstring).

(* --- Processes --- *)
let Verifier_Test(vID:bitstring,ipID:bitstring) =
	in(cP,inV1:bitstring);
	let nP = Proj1(Decrypt(inV1,sKey(vID))) in
	let sP = Proj2(Proj2(Decrypt(inV1,sKey(vID)))) in
	let TestIdV = equals(vID,Proj1(Proj2(Decrypt(inV1,sKey(vID))))) in	
	let TestSign = equals(nP,Verify(sP,spKey(ipID))) in
	new nV:bitstring;
	new mV:bitstring;
	out(cP,Pair(nV,mV));
	let RV0 = PRF(Pair(nP,nV)) in
	let RV1 = XOR(nP,XOR(mV,RV0)) in
	new cV:bitstring;	
	phase 1;	
	out(cP,cV);
	in(cP,inV2:bitstring);
	phase 2;
	in(cP,inV3:bitstring); 
	let Tr = PRF(Pair(nP,Pair(nV,Pair(mV,Pair(cV,inV2))))) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let TestTr = equals(Tr,inV3) in
	event EndV(vID,ipID).
	
let Verifier_P0(vID:bitstring,ipID:bitstring) =
	in(cP,inV1:bitstring);
	let nP = Proj1(Decrypt(inV1,sKey(vID))) in
	let sP = Proj2(Proj2(Decrypt(inV1,sKey(vID)))) in
	let TestIdV = equals(vID,Proj1(Proj2(Decrypt(inV1,sKey(vID))))) in	
	let TestSign = equals(nP,Verify(sP,spKey(ipID))) in
	new nV:bitstring;
	new mV:bitstring;
	out(cP,Pair(nV,mV));
	let RV0 = PRF(Pair(nP,nV)) in
	let RV1 = XOR(nP,XOR(mV,RV0)) in	
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	in(cP,inV3:bitstring); 
	let Tr = PRF(Pair(nP,Pair(nV,Pair(mV,Pair(cV,inV2))))) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let TestTr = equals(Tr,inV3) in
	0.
	
let Verifier_P0P0P1(vID:bitstring,ipID:bitstring) =
	in(cP,inV1:bitstring);
	let nP = Proj1(Decrypt(inV1,sKey(vID))) in
	let sP = Proj2(Proj2(Decrypt(inV1,sKey(vID)))) in
	let TestIdV = equals(vID,Proj1(Proj2(Decrypt(inV1,sKey(vID))))) in	
	let TestSign = equals(nP,Verify(sP,spKey(ipID))) in
	new nV:bitstring;
	new mV:bitstring;
	out(cP,Pair(nV,mV));
	let RV0 = PRF(Pair(nP,nV)) in
	let RV1 = XOR(nP,XOR(mV,RV0)) in	
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	phase 1;
	in(cP,inV3:bitstring); 
	let Tr = PRF(Pair(nP,Pair(nV,Pair(mV,Pair(cV,inV2))))) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let TestTr = equals(Tr,inV3) in
	0.
	
let Verifier_P0P0P2(vID:bitstring,ipID:bitstring) =
	in(cP,inV1:bitstring);
	let nP = Proj1(Decrypt(inV1,sKey(vID))) in
	let sP = Proj2(Proj2(Decrypt(inV1,sKey(vID)))) in
	let TestIdV = equals(vID,Proj1(Proj2(Decrypt(inV1,sKey(vID))))) in	
	let TestSign = equals(nP,Verify(sP,spKey(ipID))) in
	new nV:bitstring;
	new mV:bitstring;
	out(cP,Pair(nV,mV));
	let RV0 = PRF(Pair(nP,nV)) in
	let RV1 = XOR(nP,XOR(mV,RV0)) in	
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	phase 2;
	in(cP,inV3:bitstring); 
	let Tr = PRF(Pair(nP,Pair(nV,Pair(mV,Pair(cV,inV2))))) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let TestTr = equals(Tr,inV3) in
	0.	
	
let Verifier_P0P1P1(vID:bitstring,ipID:bitstring) =
	in(cP,inV1:bitstring);
	let nP = Proj1(Decrypt(inV1,sKey(vID))) in
	let sP = Proj2(Proj2(Decrypt(inV1,sKey(vID)))) in
	let TestIdV = equals(vID,Proj1(Proj2(Decrypt(inV1,sKey(vID))))) in	
	let TestSign = equals(nP,Verify(sP,spKey(ipID))) in
	new nV:bitstring;
	new mV:bitstring;
	out(cP,Pair(nV,mV));
	let RV0 = PRF(Pair(nP,nV)) in
	let RV1 = XOR(nP,XOR(mV,RV0)) in	
	new cV:bitstring;
	out(cP,cV);
	phase 1;
	in(cP,inV2:bitstring);
	in(cP,inV3:bitstring); 
	let Tr = PRF(Pair(nP,Pair(nV,Pair(mV,Pair(cV,inV2))))) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let TestTr = equals(Tr,inV3) in
	0.	

let Verifier_P0P1P2(vID:bitstring,ipID:bitstring) =
	in(cP,inV1:bitstring);
	let nP = Proj1(Decrypt(inV1,sKey(vID))) in
	let sP = Proj2(Proj2(Decrypt(inV1,sKey(vID)))) in
	let TestIdV = equals(vID,Proj1(Proj2(Decrypt(inV1,sKey(vID))))) in	
	let TestSign = equals(nP,Verify(sP,spKey(ipID))) in
	new nV:bitstring;
	new mV:bitstring;
	out(cP,Pair(nV,mV));
	let RV0 = PRF(Pair(nP,nV)) in
	let RV1 = XOR(nP,XOR(mV,RV0)) in	
	new cV:bitstring;
	out(cP,cV);
	phase 1;
	in(cP,inV2:bitstring);
	phase 2;
	in(cP,inV3:bitstring); 
	let Tr = PRF(Pair(nP,Pair(nV,Pair(mV,Pair(cV,inV2))))) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let TestTr = equals(Tr,inV3) in
	0.	

let Verifier_P0P2P2(vID:bitstring,ipID:bitstring) =
	in(cP,inV1:bitstring);
	let nP = Proj1(Decrypt(inV1,sKey(vID))) in
	let sP = Proj2(Proj2(Decrypt(inV1,sKey(vID)))) in
	let TestIdV = equals(vID,Proj1(Proj2(Decrypt(inV1,sKey(vID))))) in	
	let TestSign = equals(nP,Verify(sP,spKey(ipID))) in
	new nV:bitstring;
	new mV:bitstring;
	out(cP,Pair(nV,mV));
	let RV0 = PRF(Pair(nP,nV)) in
	let RV1 = XOR(nP,XOR(mV,RV0)) in	
	new cV:bitstring;
	out(cP,cV);
	phase 2;
	in(cP,inV2:bitstring);
	in(cP,inV3:bitstring); 
	let Tr = PRF(Pair(nP,Pair(nV,Pair(mV,Pair(cV,inV2))))) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let TestTr = equals(Tr,inV3) in
	0.		
	
let Verifier_P1(vID:bitstring,ipID:bitstring) =
	phase 1;
	in(cP,inV1:bitstring);
	let nP = Proj1(Decrypt(inV1,sKey(vID))) in
	let sP = Proj2(Proj2(Decrypt(inV1,sKey(vID)))) in
	let TestIdV = equals(vID,Proj1(Proj2(Decrypt(inV1,sKey(vID))))) in	
	let TestSign = equals(nP,Verify(sP,spKey(ipID))) in
	new nV:bitstring;
	new mV:bitstring;
	out(cP,Pair(nV,mV));
	let RV0 = PRF(Pair(nP,nV)) in
	let RV1 = XOR(nP,XOR(mV,RV0)) in	
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	in(cP,inV3:bitstring); 
	let Tr = PRF(Pair(nP,Pair(nV,Pair(mV,Pair(cV,inV2))))) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let TestTr = equals(Tr,inV3) in
	0.	

let Verifier_P1P1P2(vID:bitstring,ipID:bitstring) =
	phase 1;
	in(cP,inV1:bitstring);
	let nP = Proj1(Decrypt(inV1,sKey(vID))) in
	let sP = Proj2(Proj2(Decrypt(inV1,sKey(vID)))) in
	let TestIdV = equals(vID,Proj1(Proj2(Decrypt(inV1,sKey(vID))))) in	
	let TestSign = equals(nP,Verify(sP,spKey(ipID))) in
	new nV:bitstring;
	new mV:bitstring;
	out(cP,Pair(nV,mV));
	let RV0 = PRF(Pair(nP,nV)) in
	let RV1 = XOR(nP,XOR(mV,RV0)) in	
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	phase 2;
	in(cP,inV3:bitstring); 
	let Tr = PRF(Pair(nP,Pair(nV,Pair(mV,Pair(cV,inV2))))) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let TestTr = equals(Tr,inV3) in
	0.		
	
let Verifier_P1P2P2(vID:bitstring,ipID:bitstring) =
	phase 1;
	in(cP,inV1:bitstring);
	let nP = Proj1(Decrypt(inV1,sKey(vID))) in
	let sP = Proj2(Proj2(Decrypt(inV1,sKey(vID)))) in
	let TestIdV = equals(vID,Proj1(Proj2(Decrypt(inV1,sKey(vID))))) in	
	let TestSign = equals(nP,Verify(sP,spKey(ipID))) in
	new nV:bitstring;
	new mV:bitstring;
	out(cP,Pair(nV,mV));
	let RV0 = PRF(Pair(nP,nV)) in
	let RV1 = XOR(nP,XOR(mV,RV0)) in	
	new cV:bitstring;
	out(cP,cV);
	phase 2;
	in(cP,inV2:bitstring);
	in(cP,inV3:bitstring); 
	let Tr = PRF(Pair(nP,Pair(nV,Pair(mV,Pair(cV,inV2))))) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let TestTr = equals(Tr,inV3) in
	0.		
	
let Verifier_P2(vID:bitstring,ipID:bitstring) =
	phase 2;
	in(cP,inV1:bitstring);
	let nP = Proj1(Decrypt(inV1,sKey(vID))) in
	let sP = Proj2(Proj2(Decrypt(inV1,sKey(vID)))) in
	let TestIdV = equals(vID,Proj1(Proj2(Decrypt(inV1,sKey(vID))))) in	
	let TestSign = equals(nP,Verify(sP,spKey(ipID))) in
	new nV:bitstring;
	new mV:bitstring;
	out(cP,Pair(nV,mV));
	let RV0 = PRF(Pair(nP,nV)) in
	let RV1 = XOR(nP,XOR(mV,RV0)) in	
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	in(cP,inV3:bitstring); 
	let Tr = PRF(Pair(nP,Pair(nV,Pair(mV,Pair(cV,inV2))))) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let TestTr = equals(Tr,inV3) in
	0.		

let Prover_P0(pID:bitstring,ivID:bitstring) =
	new nP:bitstring;
	let sP = Sign(nP,ssKey(pID)) in
	let eP = Encrypt(Pair(nP,Pair(ivID,sP)),pKey(ivID)) in
	out(cP, eP);
	in(cP,inP1:bitstring);
	let nV = Proj1(inP1) in
	let mV = Proj2(inP1) in
	let RP0 = PRF(Pair(nP,nV)) in
	let RP1 = XOR(nP,XOR(mV,RP0)) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let T = PRF(Pair(nP,Pair(nV,Pair(mV,Pair(inP2,Ans))))) in
	out(cP,T);
	0.
	
let Prover_P0P1(pID:bitstring,ivID:bitstring) =
	new nP:bitstring;
	let sP = Sign(nP,ssKey(pID)) in
	let eP = Encrypt(Pair(nP,Pair(ivID,sP)),pKey(ivID)) in
	out(cP, eP);
	in(cP,inP1:bitstring);
	let nV = Proj1(inP1) in
	let mV = Proj2(inP1) in
	let RP0 = PRF(Pair(nP,nV)) in
	let RP1 = XOR(nP,XOR(mV,RP0)) in
	phase 1;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let T = PRF(Pair(nP,Pair(nV,Pair(mV,Pair(inP2,Ans))))) in
	out(cP,T);
	0.	
	
let Prover_P0P2(pID:bitstring,ivID:bitstring) =
	new nP:bitstring;
	let sP = Sign(nP,ssKey(pID)) in
	let eP = Encrypt(Pair(nP,Pair(ivID,sP)),pKey(ivID)) in
	out(cP, eP);
	in(cP,inP1:bitstring);
	let nV = Proj1(inP1) in
	let mV = Proj2(inP1) in
	let RP0 = PRF(Pair(nP,nV)) in
	let RP1 = XOR(nP,XOR(mV,RP0)) in
	phase 2;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let T = PRF(Pair(nP,Pair(nV,Pair(mV,Pair(inP2,Ans))))) in
	out(cP,T);
	0.	
	
let Prover_P1(pID:bitstring,ivID:bitstring) =
	new nP:bitstring;
	let sP = Sign(nP,ssKey(pID)) in
	let eP = Encrypt(Pair(nP,Pair(ivID,sP)),pKey(ivID)) in
	out(cP, eP);
	phase 1;	
	in(cP,inP1:bitstring);
	let nV = Proj1(inP1) in
	let mV = Proj2(inP1) in
	let RP0 = PRF(Pair(nP,nV)) in
	let RP1 = XOR(nP,XOR(mV,RP0)) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let T = PRF(Pair(nP,Pair(nV,Pair(mV,Pair(inP2,Ans))))) in
	out(cP,T);
	0.	
	
let Prover_P1P2(pID:bitstring,ivID:bitstring) =
	new nP:bitstring;
	let sP = Sign(nP,ssKey(pID)) in
	let eP = Encrypt(Pair(nP,Pair(ivID,sP)),pKey(ivID)) in
	out(cP, eP);
	phase 1;	
	in(cP,inP1:bitstring);
	let nV = Proj1(inP1) in
	let mV = Proj2(inP1) in
	let RP0 = PRF(Pair(nP,nV)) in
	let RP1 = XOR(nP,XOR(mV,RP0)) in
	phase 2;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let T = PRF(Pair(nP,Pair(nV,Pair(mV,Pair(inP2,Ans))))) in
	out(cP,T);
	0.
	
let Prover_P2(pID:bitstring,ivID:bitstring) =
	new nP:bitstring;
	let sP = Sign(nP,ssKey(pID)) in
	let eP = Encrypt(Pair(nP,Pair(ivID,sP)),pKey(ivID)) in
	out(cP, eP);
	phase 2;
	in(cP,inP1:bitstring);
	let nV = Proj1(inP1) in
	let mV = Proj2(inP1) in
	let RP0 = PRF(Pair(nP,nV)) in
	let RP1 = XOR(nP,XOR(mV,RP0)) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let T = PRF(Pair(nP,Pair(nV,Pair(mV,Pair(inP2,Ans))))) in
	out(cP,T);
	0.	
				
(* --- Security Property --- *)
query event(EndV(idV0,idP0)).

(* --- Global Process --- *)
process
	(* Secrets known to the adversary *)
	out(cP,sKey(idP0));
	out(cP,ssKey(idP0));	
	(* Now we let the adversary (almost) alone *)
	(* Honest Verifier used to test the DHR property *)
	Verifier_Test(idV0,idP0)|	
	(* Processes that can also speak during the execution : *)
	(* idV0 may execute Prover/Verifier roles at anytime *)
	!Verifier_P0(idV0,idV0) | !Verifier_P1(idV0,idV0) | !Verifier_P2(idV0,idV0) |
	!Verifier_P0P0P1(idV0,idV0) | !Verifier_P0P0P2(idV0,idV0) | !Verifier_P0P1P1(idV0,idV0) |	
	!Verifier_P0P1P2(idV0,idV0) | !Verifier_P0P2P2(idV0,idV0) | 
	!Verifier_P1P1P2(idV0,idV0) | !Verifier_P1P2P2(idV0,idV0) |
	!Verifier_P0(idV0,idP0) | !Verifier_P1(idV0,idP0) | !Verifier_P2(idV0,idP0) |
	!Verifier_P0P0P1(idV0,idP0) | !Verifier_P0P0P2(idV0,idP0) | !Verifier_P0P1P1(idV0,idP0) |	
	!Verifier_P0P1P2(idV0,idP0) | !Verifier_P0P2P2(idV0,idP0) | 
	!Verifier_P1P1P2(idV0,idP0) | !Verifier_P1P2P2(idV0,idP0) |
	!Verifier_P0(idV0,idE0) | !Verifier_P1(idV0,idE0) | !Verifier_P2(idV0,idE0) |
	!Verifier_P0P0P1(idV0,idE0) | !Verifier_P0P0P2(idV0,idE0) | !Verifier_P0P1P1(idV0,idE0) |	
	!Verifier_P0P1P2(idV0,idE0) | !Verifier_P0P2P2(idV0,idE0) | 
	!Verifier_P1P1P2(idV0,idE0) | !Verifier_P1P2P2(idV0,idE0) |	
	!Prover_P0(idV0,idV0) | !Prover_P1(idV0,idV0) | !Prover_P2(idV0,idV0) |
	!Prover_P0P1(idV0,idV0) | !Prover_P0P2(idV0,idV0) | !Prover_P1P2(idV0,idV0) |	
	!Prover_P0(idV0,idP0) | !Prover_P1(idV0,idP0) | !Prover_P2(idV0,idP0) |
	!Prover_P0P1(idV0,idP0) | !Prover_P0P2(idV0,idP0) | !Prover_P1P2(idV0,idP0) |	
	!Prover_P0(idV0,idE0) | !Prover_P1(idV0,idE0) | !Prover_P2(idV0,idE0) |
	!Prover_P0P1(idV0,idE0) | !Prover_P0P2(idV0,idE0) | !Prover_P1P2(idV0,idE0) |
	(* idE0 only speaks in P0/P2 *)
	!Verifier_P0(idE0,idV0) | !Verifier_P2(idE0,idV0) | !Verifier_P0P0P2(idE0,idV0) | !Verifier_P0P2P2(idE0,idV0) |
	!Verifier_P0(idE0,idP0) | !Verifier_P2(idE0,idP0) | !Verifier_P0P0P2(idE0,idP0) | !Verifier_P0P2P2(idE0,idP0) |
	!Verifier_P0(idE0,idE0) | !Verifier_P2(idE0,idE0) | !Verifier_P0P0P2(idE0,idE0) | !Verifier_P0P2P2(idE0,idE0) |	
	!Prover_P0(idE0,idV0) | !Prover_P2(idE0,idV0) | !Prover_P0P2(idE0,idV0) |
	!Prover_P0(idE0,idP0) | !Prover_P2(idE0,idP0) | !Prover_P0P2(idE0,idP0) |	
	!Prover_P0(idE0,idE0) | !Prover_P2(idE0,idE0) | !Prover_P0P2(idE0,idE0)	