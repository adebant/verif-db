(* ----------------------------------------- *)
(*               TREAD PROTOCOL              *)
(*             Shared Key Version            *)
(*          Terrorist Fraud Analysis         *)
(* ----------------------------------------- *)

(* -- Protocol Scheme -- *)
(* P,V share a secret key (shk1,shk2) for encryption and MAC.
P -> V : <idP,Encrypt(<aP,bP,macP>,shk1>
V -> P : mV
V -> P : cV
P -> V : Answer(cV,aP,XOR(bP,mV))
*)

(* -- Signature --- *)
const OK:bitstring.

(* Shared key *)
fun sharedKeyEnc(bitstring,bitstring):bitstring [private].
fun sharedKeyMac(bitstring,bitstring):bitstring [private].

(* Symetric encryption *)
fun Encrypt(bitstring,bitstring):bitstring.
fun Decrypt(bitstring,bitstring):bitstring
reduc forall x:bitstring, y:bitstring; Decrypt(Encrypt(x,y),y) = x.

(* Signature *)
fun Mac(bitstring,bitstring):bitstring.

(* Weak XOR *)
fun XOR(bitstring,bitstring):bitstring.
reduc forall x:bitstring, y:bitstring; commutXOR(XOR(x,y)) = XOR(y,x).
reduc forall x:bitstring, y:bitstring; cancelXOR1(XOR(x,y),x) = y.
reduc forall x:bitstring, y:bitstring; cancelXOR2(XOR(x,y),y) = x.
reduc forall x:bitstring, y:bitstring, z:bitstring; cancelTXOR1(y,XOR(x,XOR(y,z))) = XOR(x,z).
reduc forall x:bitstring, y:bitstring, z:bitstring; cancelTXOR2(z,XOR(x,XOR(y,z))) = XOR(x,y).

(* Equality Check Function *)
fun equals(bitstring,bitstring):bitstring
reduc forall x:bitstring; equals(x,x) = OK.

(* Answering function *)
fun Answer(bitstring,bitstring,bitstring):bitstring.

(* --- Constants / Channels --- *)
free cP:channel.
free idV0:bitstring. (* Honest Player acting as Test Verifier *)
free idP0:bitstring. (* Honest Player far-away from idV0 *)
free idVC:bitstring. (* Dishonest Player close to idV0 *)
free idPC:bitstring. (* Dishonest Player close to idP0 *)

(* --- Events --- *)
event EndV(bitstring,bitstring).

(* --- Processes --- *)
let Verifier_Test(vID:bitstring,ipID:bitstring) =
	in(cP,(inV1:bitstring,=ipID));
	let (aP:bitstring,bP:bitstring,macP:bitstring) = Decrypt(inV1,sharedKeyEnc(vID,ipID)) in
	let TestMac = equals(macP,Mac((aP,bP),sharedKeyMac(vID,ipID))) in
	new mV:bitstring;
	out(cP,mV);
	let RV0 = aP in
	let RV1 = XOR(bP,mV) in	
	new cV:bitstring;	
	phase 1;	
	out(cP,cV);
	in(cP,inV3:bitstring);
	phase 2;
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV3) in
	event EndV(vID,ipID).
	
let Verifier_P0(vID:bitstring,ipID:bitstring) =
	in(cP,(inV1:bitstring,=ipID));
	let (aP:bitstring,bP:bitstring,macP:bitstring) = Decrypt(inV1,sharedKeyEnc(vID,ipID)) in
	let TestMac = equals(macP,Mac((aP,bP),sharedKeyMac(vID,ipID))) in
	new mV:bitstring;
	out(cP,mV);
	let RV0 = aP in
	let RV1 = XOR(bP,mV) in		
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV3:bitstring);
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV3) in
	0.

let Verifier_P1(vID:bitstring,ipID:bitstring) = 
	phase 1;
	in(cP,(inV1:bitstring,=ipID));
	let (aP:bitstring,bP:bitstring,macP:bitstring) = Decrypt(inV1,sharedKeyEnc(vID,ipID)) in
	let TestMac = equals(macP,Mac((aP,bP),sharedKeyMac(vID,ipID))) in
	new mV:bitstring;
	out(cP,mV);
	let RV0 = aP in
	let RV1 = XOR(bP,mV) in		
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV3:bitstring);
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV3) in
	0.
	
let Verifier_P2(vID:bitstring,ipID:bitstring) = 
	phase 2;
	in(cP,(inV1:bitstring,=ipID));
	let (aP:bitstring,bP:bitstring,macP:bitstring) = Decrypt(inV1,sharedKeyEnc(vID,ipID)) in
	let TestMac = equals(macP,Mac((aP,bP),sharedKeyMac(vID,ipID))) in
	new mV:bitstring;
	out(cP,mV);
	let RV0 = aP in
	let RV1 = XOR(bP,mV) in		
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV3:bitstring);
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV3) in
	0.
	
let Verifier_P0P1(vID:bitstring,ipID:bitstring) =
	in(cP,(inV1:bitstring,=ipID));
	let (aP:bitstring,bP:bitstring,macP:bitstring) = Decrypt(inV1,sharedKeyEnc(vID,ipID)) in
	let TestMac = equals(macP,Mac((aP,bP),sharedKeyMac(vID,ipID))) in
	new mV:bitstring;
	out(cP,mV);
	let RV0 = aP in
	let RV1 = XOR(bP,mV) in		
	new cV:bitstring;
	out(cP,cV);
	phase 1;
	in(cP,inV3:bitstring);
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV3) in
	0.
	
let Verifier_P0P2(vID:bitstring,ipID:bitstring) = 	
	in(cP,(inV1:bitstring,=ipID));
	let (aP:bitstring,bP:bitstring,macP:bitstring) = Decrypt(inV1,sharedKeyEnc(vID,ipID)) in
	let TestMac = equals(macP,Mac((aP,bP),sharedKeyMac(vID,ipID))) in
	new mV:bitstring;
	out(cP,mV);
	let RV0 = aP in
	let RV1 = XOR(bP,mV) in		
	new cV:bitstring;
	out(cP,cV);
	phase 2;
	in(cP,inV3:bitstring);
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV3) in
	0.
	
let Verifier_P1P2(vID:bitstring,ipID:bitstring) =
	phase 1;
	in(cP,(inV1:bitstring,=ipID));
	let (aP:bitstring,bP:bitstring,macP:bitstring) = Decrypt(inV1,sharedKeyEnc(vID,ipID)) in
	let TestMac = equals(macP,Mac((aP,bP),sharedKeyMac(vID,ipID))) in
	new mV:bitstring;
	out(cP,mV);
	let RV0 = aP in
	let RV1 = XOR(bP,mV) in		
	new cV:bitstring;
	out(cP,cV);
	phase 2;
	in(cP,inV3:bitstring);
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV3) in
	0.
	
let Prover_P0(pID:bitstring,ivID:bitstring) =	
	new aP:bitstring;
	new bP:bitstring;
	let macP = Mac((aP,bP),sharedKeyMac(ivID,pID)) in
	let eP = Encrypt((aP,bP,macP),sharedKeyEnc(ivID,pID)) in
	out(cP, (eP,pID));
	in(cP,inP1:bitstring);
	let RP0 = aP in
	let RP1 = XOR(bP,inP1) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans).
	
let Prover_P1(pID:bitstring,ivID:bitstring) =
	new aP:bitstring;
	new bP:bitstring;
	let macP = Mac((aP,bP),sharedKeyMac(ivID,pID)) in
	let eP = Encrypt((aP,bP,macP),sharedKeyEnc(ivID,pID)) in
	out(cP, (eP,pID));
	phase 1;	
	in(cP,inP1:bitstring);
	let RP0 = aP in
	let RP1 = XOR(bP,inP1) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans).
	
let Prover_P2(pID:bitstring,ivID:bitstring) =
	new aP:bitstring;
	new bP:bitstring;
	let macP = Mac((aP,bP),sharedKeyMac(ivID,pID)) in
	let eP = Encrypt((aP,bP,macP),sharedKeyEnc(ivID,pID)) in
	out(cP, (eP,pID));
	phase 2;	
	in(cP,inP1:bitstring);
	let RP0 = aP in
	let RP1 = XOR(bP,inP1) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans).

let Prover_P0P1(pID:bitstring,ivID:bitstring) =
	new aP:bitstring;
	new bP:bitstring;
	let macP = Mac((aP,bP),sharedKeyMac(ivID,pID)) in
	let eP = Encrypt((aP,bP,macP),sharedKeyEnc(ivID,pID)) in
	out(cP, (eP,pID));
	in(cP,inP1:bitstring);
	let RP0 = aP in
	let RP1 = XOR(bP,inP1) in
	phase 1;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans).

let Prover_P0P2(pID:bitstring,ivID:bitstring) =	
	new aP:bitstring;
	new bP:bitstring;
	let macP = Mac((aP,bP),sharedKeyMac(ivID,pID)) in
	let eP = Encrypt((aP,bP,macP),sharedKeyEnc(ivID,pID)) in
	out(cP, (eP,pID));
	in(cP,inP1:bitstring);
	let RP0 = aP in
	let RP1 = XOR(bP,inP1) in
	phase 2;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans).

let Prover_P1P2(pID:bitstring,ivID:bitstring) =	
	new aP:bitstring;
	new bP:bitstring;
	let macP = Mac((aP,bP),sharedKeyMac(ivID,pID)) in
	let eP = Encrypt((aP,bP,macP),sharedKeyEnc(ivID,pID)) in
	out(cP, (eP,pID));
	phase 1;	
	in(cP,inP1:bitstring);
	let RP0 = aP in
	let RP1 = XOR(bP,inP1) in
	phase 2;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans).
	 
(* --- Security Property --- *)
query event(EndV(idV0,idP0)).

(* --- Prover Oracle --- *)
(*
let Prover_Oracle(pID:bitstring,ivID:bitstring) =
	new aP:bitstring;
	new bP:bitstring;
	let macP = Mac((aP,bP),sharedKeyMac(ivID,pID)) in
	let eP = Encrypt((aP,bP,macP),sharedKeyEnc(ivID,pID)) in
	out(cP, (eP,pID));
	in(cP,inP1:bitstring);
	let RP0 = aP in
	let RP1 = XOR(bP,inP1) in
	out(cP,(RP0,RP1));
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans).
*)

(* --- Global Process --- *)
process
	(* Secrets known to the adversary *)
	out(cP,(sharedKeyEnc(idV0,idVC),sharedKeyMac(idV0,idVC)));
	out(cP,(sharedKeyEnc(idV0,idPC),sharedKeyMac(idV0,idPC)));
	out(cP,(sharedKeyEnc(idP0,idVC),sharedKeyMac(idP0,idVC)));
	out(cP,(sharedKeyEnc(idP0,idPC),sharedKeyMac(idP0,idPC)));	
	out(cP,(sharedKeyEnc(idVC,idV0),sharedKeyMac(idVC,idV0)));
	out(cP,(sharedKeyEnc(idVC,idP0),sharedKeyMac(idVC,idP0)));
	out(cP,(sharedKeyEnc(idVC,idVC),sharedKeyMac(idVC,idVC)));
	out(cP,(sharedKeyEnc(idVC,idPC),sharedKeyMac(idVC,idPC)));	
	out(cP,(sharedKeyEnc(idPC,idV0),sharedKeyMac(idPC,idV0)));
	out(cP,(sharedKeyEnc(idPC,idP0),sharedKeyMac(idPC,idP0)));
	out(cP,(sharedKeyEnc(idPC,idVC),sharedKeyMac(idPC,idVC)));
	out(cP,(sharedKeyEnc(idPC,idPC),sharedKeyMac(idPC,idPC)));
	(* We provide the adversary with the content of the frame obtained by running the Prover_Oracle with the Verifier *)
	new aPone:bitstring;
	new bPone:bitstring;
	new mVone:bitstring;	
	new cVone:bitstring;
	let macPone = Mac((aPone,bPone),sharedKeyMac(idV0,idP0)) in
	let ePone = Encrypt((aPone,bPone,macPone),sharedKeyEnc(idV0,idP0)) in
	out(cP,(ePone,idP0));
	out(cP,mVone);
	let RP0one = aPone in
	let RP1one = XOR(bPone,mVone) in
	out(cP,(RP0one,RP1one));
	out(cP,cVone);	
	(* Now we let the adversary (almost) alone *)
	(* Honest Verifier used to test the TFR property *)
	Verifier_Test(idV0,idP0) |
	(* Processes that can also speak during the execution : *)
	(* idV0 may execute Prover/Verifier roles at anytime *)
	!Verifier_P0(idV0,idV0) | !Verifier_P1(idV0,idV0) | !Verifier_P2(idV0,idV0) |
	!Verifier_P0P1(idV0,idV0) | !Verifier_P0P2(idV0,idV0) | !Verifier_P1P2(idV0,idV0) |	
	!Verifier_P0(idV0,idP0) | !Verifier_P1(idV0,idP0) | !Verifier_P2(idV0,idP0) |
	!Verifier_P0P1(idV0,idP0) | !Verifier_P0P2(idV0,idP0) | !Verifier_P1P2(idV0,idP0) |		
	!Verifier_P0(idV0,idVC) | !Verifier_P1(idV0,idVC) | !Verifier_P2(idV0,idVC) |
	!Verifier_P0P1(idV0,idVC) | !Verifier_P0P2(idV0,idVC) | !Verifier_P1P2(idV0,idVC) |
	!Verifier_P0(idV0,idPC) | !Verifier_P1(idV0,idPC) | !Verifier_P2(idV0,idPC) |
	!Verifier_P0P1(idV0,idPC) | !Verifier_P0P2(idV0,idPC) | !Verifier_P1P2(idV0,idPC) |
	!Prover_P0(idV0,idV0) | !Prover_P1(idV0,idV0) | !Prover_P2(idV0,idV0) |
	!Prover_P0P1(idV0,idV0) | !Prover_P0P2(idV0,idV0) | !Prover_P1P2(idV0,idV0) |
	!Prover_P0(idV0,idP0) | !Prover_P1(idV0,idP0) | !Prover_P2(idV0,idP0) |
	!Prover_P0P1(idV0,idP0) | !Prover_P0P2(idV0,idP0) | !Prover_P1P2(idV0,idP0) |		
	!Prover_P0(idV0,idVC) | !Prover_P1(idV0,idVC) | !Prover_P2(idV0,idVC) |
	!Prover_P0P1(idV0,idVC) | !Prover_P0P2(idV0,idVC) | !Prover_P1P2(idV0,idVC) |
	!Prover_P0(idV0,idPC) | !Prover_P1(idV0,idPC) | !Prover_P2(idV0,idPC) |
	!Prover_P0P1(idV0,idPC) | !Prover_P0P2(idV0,idPC) | !Prover_P1P2(idV0,idPC) |	
	(* idP0 only speaks in P0/P2 *)
	!Verifier_P0(idP0,idV0) | !Verifier_P2(idP0,idV0) | !Verifier_P0P2(idP0,idV0) |
	!Verifier_P0(idP0,idP0) | !Verifier_P2(idP0,idP0) | !Verifier_P0P2(idP0,idP0) |	
	!Verifier_P0(idP0,idVC) | !Verifier_P2(idP0,idVC) | !Verifier_P0P2(idP0,idVC) |
	!Verifier_P0(idP0,idPC) | !Verifier_P2(idP0,idPC) | !Verifier_P0P2(idP0,idPC) |
	!Prover_P0(idP0,idV0) | !Prover_P2(idP0,idV0) | !Prover_P0P2(idP0,idV0) |
	!Prover_P0(idP0,idP0) | !Prover_P2(idP0,idP0) | !Prover_P0P2(idP0,idP0) |	
	!Prover_P0(idP0,idVC) | !Prover_P2(idP0,idVC) | !Prover_P0P2(idP0,idVC) |
	!Prover_P0(idP0,idPC) | !Prover_P2(idP0,idPC) | !Prover_P0P2(idP0,idPC)	