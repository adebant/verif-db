(* ----------------------------------------- *)
(*               TREAD PROTOCOL              *)
(*    Version idpub = null / idpriv = idP    *)
(*            Mafia Fraud Analysis           *)
(* ----------------------------------------- *)

(* -- Protocol Scheme -- *)
(* P,V have secret/public keys for encryption and signature.
P -> V : Encrypt(<aP,bP,idP,sP>,pkV)
V -> P : mV
V -> P : cV
P -> V : Answer(cV,aP,XOR(bP,mV))
*)

(* -- Signature --- *)
const OK:bitstring.

(* Public-key encryption *)
fun sKey(bitstring):bitstring [private].
fun pKey(bitstring):bitstring.

fun Encrypt(bitstring,bitstring):bitstring.
fun Decrypt(bitstring,bitstring):bitstring
reduc forall x:bitstring, y:bitstring; Decrypt(Encrypt(x,pKey(y)),sKey(y)) = x.

(* Signature *)
fun ssKey(bitstring):bitstring [private].
fun spKey(bitstring):bitstring.

fun Sign(bitstring,bitstring):bitstring.
fun Verify(bitstring,bitstring):bitstring
reduc forall x:bitstring, y:bitstring; Verify(Sign(x,ssKey(y)),spKey(y)) = x.

(* Weak XOR *)
fun XOR(bitstring,bitstring):bitstring.
reduc forall x:bitstring, y:bitstring; commutXOR(XOR(x,y)) = XOR(y,x).
reduc forall x:bitstring, y:bitstring; cancelXOR1(XOR(x,y),x) = y.
reduc forall x:bitstring, y:bitstring; cancelXOR2(XOR(x,y),y) = x.
reduc forall x:bitstring, y:bitstring, z:bitstring; cancelTXOR1(y,XOR(x,XOR(y,z))) = XOR(x,z).
reduc forall x:bitstring, y:bitstring, z:bitstring; cancelTXOR2(z,XOR(x,XOR(y,z))) = XOR(x,y).

(* Equality Check Function *)
fun equals(bitstring,bitstring):bitstring
reduc forall x:bitstring; equals(x,x) = OK.

(* Answering function *)
fun Answer(bitstring,bitstring,bitstring):bitstring.

(* --- Constants / Channels --- *)
free cP:channel.
free idV0:bitstring. (* Honest Player acting as Test Verifier *)
free idP0:bitstring. (* Honest Player far-away from idV0 *)
free idVC:bitstring. (* Dishonest Player close to idV0 *)
free idPC:bitstring. (* Dishonest Player close to idP0 *)

(* --- Events --- *)
event EndV(bitstring,bitstring).

(* --- Processes --- *)
let Verifier_Test(vID:bitstring,ipID:bitstring) =
	in(cP,inV1:bitstring);
	let (aP:bitstring,bP:bitstring,=ipID,sP:bitstring) = Decrypt(inV1,sKey(vID)) in
	let TestSign = equals((aP,bP,ipID),Verify(sP,spKey(ipID))) in
	new mV:bitstring;
	out(cP,mV);
	let RV0 = aP in
	let RV1 = XOR(bP,mV) in	
	new cV:bitstring;	
	phase 1;	
	out(cP,cV);
	in(cP,inV2:bitstring);
	phase 2;
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	event EndV(vID,ipID).
	
let Verifier_P0(vID:bitstring,ipID:bitstring) =
	in(cP,inV1:bitstring);
	let (aP:bitstring,bP:bitstring,=ipID,sP:bitstring) = Decrypt(inV1,sKey(vID)) in
	let TestSign = equals((aP,bP,ipID),Verify(sP,spKey(ipID))) in
	new mV:bitstring;
	out(cP,mV);
	let RV0 = aP in
	let RV1 = XOR(bP,mV) in		
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	0.

let Verifier_P1(vID:bitstring,ipID:bitstring) =
	phase 1;
	in(cP,inV1:bitstring);
	let (aP:bitstring,bP:bitstring,=ipID,sP:bitstring) = Decrypt(inV1,sKey(vID)) in
	let TestSign = equals((aP,bP,ipID),Verify(sP,spKey(ipID))) in
	new mV:bitstring;
	out(cP,mV);
	let RV0 = aP in
	let RV1 = XOR(bP,mV) in		
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	0.
	
let Verifier_P2(vID:bitstring,ipID:bitstring) =
	phase 2;
	in(cP,inV1:bitstring);
	let (aP:bitstring,bP:bitstring,=ipID,sP:bitstring) = Decrypt(inV1,sKey(vID)) in
	let TestSign = equals((aP,bP,ipID),Verify(sP,spKey(ipID))) in
	new mV:bitstring;
	out(cP,mV);
	let RV0 = aP in
	let RV1 = XOR(bP,mV) in		
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	0.
	
let Verifier_P0P1(vID:bitstring,ipID:bitstring) =
	in(cP,inV1:bitstring);
	let (aP:bitstring,bP:bitstring,=ipID,sP:bitstring) = Decrypt(inV1,sKey(vID)) in
	let TestSign = equals((aP,bP,ipID),Verify(sP,spKey(ipID))) in
	new mV:bitstring;
	out(cP,mV);
	let RV0 = aP in
	let RV1 = XOR(bP,mV) in		
	new cV:bitstring;
	out(cP,cV);
	phase 1;
	in(cP,inV2:bitstring);
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	0.	
	
let Verifier_P0P2(vID:bitstring,ipID:bitstring) =
	in(cP,inV1:bitstring);
	let (aP:bitstring,bP:bitstring,=ipID,sP:bitstring) = Decrypt(inV1,sKey(vID)) in
	let TestSign = equals((aP,bP,ipID),Verify(sP,spKey(ipID))) in
	new mV:bitstring;
	out(cP,mV);
	let RV0 = aP in
	let RV1 = XOR(bP,mV) in		
	new cV:bitstring;
	out(cP,cV);
	phase 2;
	in(cP,inV2:bitstring);
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	0.		
	
let Verifier_P1P2(vID:bitstring,ipID:bitstring) =
	phase 1;
	in(cP,inV1:bitstring);
	let (aP:bitstring,bP:bitstring,=ipID,sP:bitstring) = Decrypt(inV1,sKey(vID)) in
	let TestSign = equals((aP,bP,ipID),Verify(sP,spKey(ipID))) in
	new mV:bitstring;
	out(cP,mV);
	let RV0 = aP in
	let RV1 = XOR(bP,mV) in		
	new cV:bitstring;
	out(cP,cV);
	phase 2;
	in(cP,inV2:bitstring);
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	0.	
	
let Prover_P0(pID:bitstring,ivID:bitstring) =	
	new aP:bitstring;
	new bP:bitstring;
	let sP = Sign((aP,bP,pID),ssKey(pID)) in
	let eP = Encrypt((aP,bP,pID,sP),pKey(ivID)) in
	out(cP, eP);
	in(cP,inP1:bitstring);
	let RP0 = aP in
	let RP1 = XOR(bP,inP1) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans).
	
let Prover_P1(pID:bitstring,ivID:bitstring) =	
	new aP:bitstring;
	new bP:bitstring;
	let sP = Sign((aP,bP,pID),ssKey(pID)) in
	let eP = Encrypt((aP,bP,pID,sP),pKey(ivID)) in
	out(cP, eP);
	phase 1;	
	in(cP,inP1:bitstring);
	let RP0 = aP in
	let RP1 = XOR(bP,inP1) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans).

let Prover_P2(pID:bitstring,ivID:bitstring) =	
	new aP:bitstring;
	new bP:bitstring;
	let sP = Sign((aP,bP,pID),ssKey(pID)) in
	let eP = Encrypt((aP,bP,pID,sP),pKey(ivID)) in
	out(cP, eP);
	phase 2;	
	in(cP,inP1:bitstring);
	let RP0 = aP in
	let RP1 = XOR(bP,inP1) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans).

let Prover_P0P1(pID:bitstring,ivID:bitstring) =	
	new aP:bitstring;
	new bP:bitstring;
	let sP = Sign((aP,bP,pID),ssKey(pID)) in
	let eP = Encrypt((aP,bP,pID,sP),pKey(ivID)) in
	out(cP, eP);
	in(cP,inP1:bitstring);
	let RP0 = aP in
	let RP1 = XOR(bP,inP1) in
	phase 1;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans).

let Prover_P0P2(pID:bitstring,ivID:bitstring) =	
	new aP:bitstring;
	new bP:bitstring;
	let sP = Sign((aP,bP,pID),ssKey(pID)) in
	let eP = Encrypt((aP,bP,pID,sP),pKey(ivID)) in
	out(cP, eP);
	in(cP,inP1:bitstring);
	let RP0 = aP in
	let RP1 = XOR(bP,inP1) in
	phase 2;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans).

let Prover_P1P2(pID:bitstring,ivID:bitstring) =	
	new aP:bitstring;
	new bP:bitstring;
	let sP = Sign((aP,bP,pID),ssKey(pID)) in
	let eP = Encrypt((aP,bP,pID,sP),pKey(ivID)) in
	out(cP, eP);
	phase 1;	
	in(cP,inP1:bitstring);
	let RP0 = aP in
	let RP1 = XOR(bP,inP1) in
	phase 2;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans).	
	
(* --- Security Property --- *)
query event(EndV(idV0,idP0)).

(* --- Global Process --- *)
process
	(* Secrets known to the adversary *)
	out(cP,(sKey(idPC),ssKey(idPC)));
	out(cP,(sKey(idVC),ssKey(idVC)));	
	(* Now we let the adversary (almost) alone *)
	(* Honest Verifier used to test the MFR property *)
	Verifier_Test(idV0,idP0) |
	(* Processes that can also speak during the execution : *)
	(* idV0 may execute Prover/Verifier roles at anytime *)
	!Verifier_P0(idV0,idV0) | !Verifier_P1(idV0,idV0) | !Verifier_P2(idV0,idV0) |
	!Verifier_P0P1(idV0,idV0) | !Verifier_P0P2(idV0,idV0) | !Verifier_P1P2(idV0,idV0) |	
	!Verifier_P0(idV0,idP0) | !Verifier_P1(idV0,idP0) | !Verifier_P2(idV0,idP0) |
	!Verifier_P0P1(idV0,idP0) | !Verifier_P0P2(idV0,idP0) | !Verifier_P1P2(idV0,idP0) |		
	!Verifier_P0(idV0,idVC) | !Verifier_P1(idV0,idVC) | !Verifier_P2(idV0,idVC) |
	!Verifier_P0P1(idV0,idVC) | !Verifier_P0P2(idV0,idVC) | !Verifier_P1P2(idV0,idVC) |
	!Verifier_P0(idV0,idPC) | !Verifier_P1(idV0,idPC) | !Verifier_P2(idV0,idPC) |
	!Verifier_P0P1(idV0,idPC) | !Verifier_P0P2(idV0,idPC) | !Verifier_P1P2(idV0,idPC) |
	!Prover_P0(idV0,idV0) | !Prover_P1(idV0,idV0) | !Prover_P2(idV0,idV0) |
	!Prover_P0P1(idV0,idV0) | !Prover_P0P2(idV0,idV0) | !Prover_P1P2(idV0,idV0) |
	!Prover_P0(idV0,idP0) | !Prover_P1(idV0,idP0) | !Prover_P2(idV0,idP0) |
	!Prover_P0P1(idV0,idP0) | !Prover_P0P2(idV0,idP0) | !Prover_P1P2(idV0,idP0) |		
	!Prover_P0(idV0,idVC) | !Prover_P1(idV0,idVC) | !Prover_P2(idV0,idVC) |
	!Prover_P0P1(idV0,idVC) | !Prover_P0P2(idV0,idVC) | !Prover_P1P2(idV0,idVC) |
	!Prover_P0(idV0,idPC) | !Prover_P1(idV0,idPC) | !Prover_P2(idV0,idPC) |
	!Prover_P0P1(idV0,idPC) | !Prover_P0P2(idV0,idPC) | !Prover_P1P2(idV0,idPC) |	
	(* idP0 only speaks in P0/P2 *)
	!Verifier_P0(idP0,idV0) | !Verifier_P2(idP0,idV0) | !Verifier_P0P2(idP0,idV0) |
	!Verifier_P0(idP0,idP0) | !Verifier_P2(idP0,idP0) | !Verifier_P0P2(idP0,idP0) |	
	!Verifier_P0(idP0,idVC) | !Verifier_P2(idP0,idVC) | !Verifier_P0P2(idP0,idVC) |
	!Verifier_P0(idP0,idPC) | !Verifier_P2(idP0,idPC) | !Verifier_P0P2(idP0,idPC) |
	!Prover_P0(idP0,idV0) | !Prover_P2(idP0,idV0) | !Prover_P0P2(idP0,idV0) |
	!Prover_P0(idP0,idP0) | !Prover_P2(idP0,idP0) | !Prover_P0P2(idP0,idP0) |	
	!Prover_P0(idP0,idVC) | !Prover_P2(idP0,idVC) | !Prover_P0P2(idP0,idVC) |
	!Prover_P0(idP0,idPC) | !Prover_P2(idP0,idPC) | !Prover_P0P2(idP0,idPC)