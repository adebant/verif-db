(* ----------------------------------------- *)
(*               CRCS PROTOCOL               *)
(*          No-revealing Signature           *)
(*           Mafia Fraud Analysis            *)
(* ----------------------------------------- *)

(* -- Protocol Scheme -- *)
(* P has a couple of public/secret key for signature purposes.
P -> V : sign(commit(nP),sk(idP))
V -> P : cV
P -> V :(cV,nP)
P -> V : (idV,nP,cV),sign((idV,nP,cV),sk(idP))
*)

(* -- Signature --- *)
const OK:bitstring.

(* Commitment *)
fun Commit(bitstring):bitstring.

(* Signature *)
fun pubKey(bitstring):bitstring.
fun secKey(bitstring):bitstring [private].
fun Sign(bitstring,bitstring):bitstring.
fun Verify(bitstring,bitstring,bitstring):bitstring
reduc forall m:bitstring, k:bitstring; Verify(m,Sign(m,secKey(k)),pubKey(k)) = OK.

(* Equality test *)
fun equals(bitstring,bitstring):bitstring
reduc forall x:bitstring; equals(x,x) = OK.

(* --- Constants / Channels --- *)
free cP:channel.
free idV0:bitstring. (* Honest Player acting as Test Verifier *)
free idP0:bitstring. (* Honest Player far-away from idV0 *)
free idVC:bitstring. (* Dishonest Player close to idV0 *)
free idPC:bitstring. (* Dishonest Player close to idP0 *)

(* --- Events --- *)
event EndV(bitstring,bitstring).

(* --- Processes --- *)
let Verifier_Test(vID:bitstring,pID:bitstring) =
	in(cP,inV1:bitstring);
	new cV:bitstring;
	phase 1;
	out(cP,cV);
	in(cP,(=cV,inV2:bitstring));
	phase 2;
	in(cP,(inV3:bitstring,inV4:bitstring));
	let TestSign1 = Verify(inV3,inV4,pubKey(pID)) in
	let (=vID,=inV2,=cV) = inV3 in
	let TestSign2 = Verify(Commit(inV2),inV1,pubKey(pID)) in
	event EndV(vID,pID).
	
let Verifier_P0(vID:bitstring,pID:bitstring) =
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	in(cP,(=cV,inV2:bitstring));
	in(cP,(inV3:bitstring,inV4:bitstring));
	let TestSign1 = Verify(inV3,inV4,pubKey(pID)) in
	let (=vID,=inV2,=cV) = inV3 in
	let TestSign2 = Verify(Commit(inV2),inV1,pubKey(pID)) in
	0.
	
let Verifier_P1(vID:bitstring,pID:bitstring) =
	phase 1;
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	in(cP,(=cV,inV2:bitstring));
	in(cP,(inV3:bitstring,inV4:bitstring));
	let TestSign1 = Verify(inV3,inV4,pubKey(pID)) in
	let (=vID,=inV2,=cV) = inV3 in
	let TestSign2 = Verify(Commit(inV2),inV1,pubKey(pID)) in
	0.

let Verifier_P2(vID:bitstring,pID:bitstring) =
	phase 2;
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	in(cP,(=cV,inV2:bitstring));
	in(cP,(inV3:bitstring,inV4:bitstring));
	let TestSign1 = Verify(inV3,inV4,pubKey(pID)) in
	let (=vID,=inV2,=cV) = inV3 in
	let TestSign2 = Verify(Commit(inV2),inV1,pubKey(pID)) in
	0.
	
let Verifier_P0P0P1(vID:bitstring,pID:bitstring) =
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	in(cP,(=cV,inV2:bitstring));
	phase 1;
	in(cP,(inV3:bitstring,inV4:bitstring));
	let TestSign1 = Verify(inV3,inV4,pubKey(pID)) in
	let (=vID,=inV2,=cV) = inV3 in
	let TestSign2 = Verify(Commit(inV2),inV1,pubKey(pID)) in
	0.
	
let Verifier_P0P0P2(vID:bitstring,pID:bitstring) =
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	in(cP,(=cV,inV2:bitstring));
	phase 2;
	in(cP,(inV3:bitstring,inV4:bitstring));
	let TestSign1 = Verify(inV3,inV4,pubKey(pID)) in
	let (=vID,=inV2,=cV) = inV3 in
	let TestSign2 = Verify(Commit(inV2),inV1,pubKey(pID)) in
	0.

let Verifier_P0P1P1(vID:bitstring,pID:bitstring) =
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	phase 1;
	in(cP,(=cV,inV2:bitstring));
	in(cP,(inV3:bitstring,inV4:bitstring));
	let TestSign1 = Verify(inV3,inV4,pubKey(pID)) in
	let (=vID,=inV2,=cV) = inV3 in
	let TestSign2 = Verify(Commit(inV2),inV1,pubKey(pID)) in
	0.

let Verifier_P0P1P2(vID:bitstring,pID:bitstring) =
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	phase 1;
	in(cP,(=cV,inV2:bitstring));
	phase 2;
	in(cP,(inV3:bitstring,inV4:bitstring));
	let TestSign1 = Verify(inV3,inV4,pubKey(pID)) in
	let (=vID,=inV2,=cV) = inV3 in
	let TestSign2 = Verify(Commit(inV2),inV1,pubKey(pID)) in
	0.

let Verifier_P0P2P2(vID:bitstring,pID:bitstring) =
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	phase 2;
	in(cP,(=cV,inV2:bitstring));
	in(cP,(inV3:bitstring,inV4:bitstring));
	let TestSign1 = Verify(inV3,inV4,pubKey(pID)) in
	let (=vID,=inV2,=cV) = inV3 in
	let TestSign2 = Verify(Commit(inV2),inV1,pubKey(pID)) in
	0.
	
let Verifier_P1P1P2(vID:bitstring,pID:bitstring) =
	phase 1;
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	in(cP,(=cV,inV2:bitstring));
	phase 2;
	in(cP,(inV3:bitstring,inV4:bitstring));
	let TestSign1 = Verify(inV3,inV4,pubKey(pID)) in
	let (=vID,=inV2,=cV) = inV3 in
	let TestSign2 = Verify(Commit(inV2),inV1,pubKey(pID)) in
	0.
	
let Verifier_P1P2P2(vID:bitstring,pID:bitstring) =
	phase 1;
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	phase 2;
	in(cP,(=cV,inV2:bitstring));
	in(cP,(inV3:bitstring,inV4:bitstring));
	let TestSign1 = Verify(inV3,inV4,pubKey(pID)) in
	let (=vID,=inV2,=cV) = inV3 in
	let TestSign2 = Verify(Commit(inV2),inV1,pubKey(pID)) in
	0.	
	
let Prover_P0(pID:bitstring,vID:bitstring) =
	new nP:bitstring;
	out(cP,Sign(Commit(nP),secKey(pID)));
	in(cP,inP1:bitstring);
	out(cP,(inP1,nP));
	out(cP,((vID,nP,inP1),Sign((vID,nP,inP1),secKey(pID)))).

let Prover_P1(pID:bitstring,vID:bitstring) =
	new nP:bitstring;
	out(cP,Sign(Commit(nP),secKey(pID)));
	phase 1;
	in(cP,inP1:bitstring);
	out(cP,(inP1,nP));
	out(cP,Sign((vID,nP,inP1),secKey(pID))).

let Prover_P2(pID:bitstring,vID:bitstring) =
	new nP:bitstring;
	out(cP,Sign(Commit(nP),secKey(pID)));
	phase 2;
	in(cP,inP1:bitstring);
	out(cP,(inP1,nP));
	out(cP,Sign((vID,nP,inP1),secKey(pID))).		

(* --- Security Property --- *)
query event(EndV(idV0,idP0)).

(* --- Global Process --- *)
process
	(* Secrets known to the adversary *)
	out(cP,secKey(idVC));
	out(cP,secKey(idPC));
	(* Now we let the adversary (almost) alone *)
	(* Honest Verifier used to test the MFR property *)
	Verifier_Test(idV0,idP0) |
	(* -- Processes that can also speak during the execution : -- *)
	(* idV0 may execute Prover/Verifier roles at anytime *)
	!Verifier_P0(idV0,idV0) | !Verifier_P1(idV0,idV0) | !Verifier_P2(idV0,idV0) |
	!Verifier_P0P0P1(idV0,idV0) | !Verifier_P0P0P2(idV0,idV0) | !Verifier_P0P1P1(idV0,idV0) |	
	!Verifier_P0P1P2(idV0,idV0) | !Verifier_P0P2P2(idV0,idV0) | 
	!Verifier_P1P1P2(idV0,idV0) | !Verifier_P1P2P2(idV0,idV0) |
	!Verifier_P0(idV0,idP0) | !Verifier_P1(idV0,idP0) | !Verifier_P2(idV0,idP0) |
	!Verifier_P0P0P1(idV0,idP0) | !Verifier_P0P0P2(idV0,idP0) | !Verifier_P0P1P1(idV0,idP0) |	
	!Verifier_P0P1P2(idV0,idP0) | !Verifier_P0P2P2(idV0,idP0) | 
	!Verifier_P1P1P2(idV0,idP0) | !Verifier_P1P2P2(idV0,idP0) |
	!Verifier_P0(idV0,idVC) | !Verifier_P1(idV0,idVC) | !Verifier_P2(idV0,idVC) |
	!Verifier_P0P0P1(idV0,idVC) | !Verifier_P0P0P2(idV0,idVC) | !Verifier_P0P1P1(idV0,idVC) |	
	!Verifier_P0P1P2(idV0,idVC) | !Verifier_P0P2P2(idV0,idVC) | 
	!Verifier_P1P1P2(idV0,idVC) | !Verifier_P1P2P2(idV0,idVC) |
	!Verifier_P0(idV0,idPC) | !Verifier_P1(idV0,idPC) | !Verifier_P2(idV0,idPC) |
	!Verifier_P0P0P1(idV0,idPC) | !Verifier_P0P0P2(idV0,idPC) | !Verifier_P0P1P1(idV0,idPC) |	
	!Verifier_P0P1P2(idV0,idPC) | !Verifier_P0P2P2(idV0,idPC) | 
	!Verifier_P1P1P2(idV0,idPC) | !Verifier_P1P2P2(idV0,idPC) |	
	!Prover_P0(idV0,idV0) | !Prover_P1(idV0,idV0) | !Prover_P2(idV0,idV0) |	
	!Prover_P0(idV0,idP0) | !Prover_P1(idV0,idP0) | !Prover_P2(idV0,idP0) |	
	!Prover_P0(idV0,idVC) | !Prover_P1(idV0,idVC) | !Prover_P2(idV0,idVC) |
	!Prover_P0(idV0,idPC) | !Prover_P1(idV0,idPC) | !Prover_P2(idV0,idPC) |
	(* idP0 only speaks in P0/P2 *)
	!Verifier_P0(idP0,idV0) | !Verifier_P2(idP0,idV0) | !Verifier_P0P0P2(idP0,idV0) | !Verifier_P0P2P2(idP0,idV0) |
	!Verifier_P0(idP0,idP0) | !Verifier_P2(idP0,idP0) | !Verifier_P0P0P2(idP0,idP0) | !Verifier_P0P2P2(idP0,idP0) |
	!Verifier_P0(idP0,idVC) | !Verifier_P2(idP0,idVC) | !Verifier_P0P0P2(idP0,idVC) | !Verifier_P0P2P2(idP0,idVC) |
	!Verifier_P0(idP0,idPC) | !Verifier_P2(idP0,idPC) | !Verifier_P0P0P2(idP0,idPC) | !Verifier_P0P2P2(idP0,idPC) |	
	!Prover_P0(idP0,idV0) | !Prover_P2(idP0,idV0) | 
	!Prover_P0(idP0,idP0) | !Prover_P2(idP0,idP0) | 
	!Prover_P0(idP0,idVC) | !Prover_P2(idP0,idVC) |
	!Prover_P0(idP0,idPC) | !Prover_P2(idP0,idPC) 