The ProVerif files contained in the three folders are used to analyze the security properties of different distance-bounding protocols.
For each protocol, we consider several security properties:
- Distance Hijacking Resistance, in the file "DHR_XXXXX.pv",
- Mafia Fraud Resistance, in the file "MFR_XXXXX.pv",
- Terrorist Fraud Resistance, in the file "TFR_XXXXX.pv".

Each ProVerif file also contains an "Alice/Bob" description of the protocol itself.

Please note that "DHR_XXXXX.pv" files need to be run with the modified 1.97 version of ProVerif (instructions below for installation)

**Results are as follows:**
(Note: ❌ stands for an attack, ✅ stands for the absence of attack.
Finally, *o.o.s.* refers to an out of scope and the corresponding file does not appear in the folder.)

| Protocol | Mafia fraud | Distance hijacking | Terrorist fraud |
| -------- | :---------: | :----------------: | :-------------: |
| Basin's Toy Example | ✅ | ✅ | ✅ |
| Brands and Chaum | ✅ | ❌ | *o.o.s.* |
| CRCS |  |  |  |
| - NoRevealing Sign | ✅ | ✅ | ❌ |
| - Revealing Sign | ✅ | ❌ | ❌ |
| Eff-PKB |  |  |  |
| - NoProtection | ✅ | ✅ | ✅ |
| - Protected | ✅ | ✅ | ✅ |
| Fiat-Shamir  | ✅ | ❌ | ❌ |
| Hancke and Kuhn | ✅ | ❌ | ❌ |
| MAD (one way) | ✅ | ❌ | *o.o.s.* |
| MasterCard-RRP | ✅ | ❌ | ❌ | 
| Meadows |  |  |  |
| F = (nV,idP,nP) | ✅ | ✅ | ❌ |
| F = nV &oplus; H(idP,nP) | ✅ | ✅ | ❌ |
| F = (nV&oplus;nP,idP) | ✅ | ✅ | ❌ |
| F = (nV,nV&oplus;idP)  | ✅ | ❌ | ❌ |
| Munilla | ✅ | ✅ | ❌ |
| NXP | ✅ | ❌ | ❌ |
| PaySafe | ✅ | ❌ | ❌ |
| SKI | ✅ | ✅ | ✅ |
| SPADE |  |  |  |
| - Original | ❌ | ❌ | ✅ |
| - Fixed | ✅ | ❌ | ✅ |
| Swiss-Knife | ✅ | ✅ | ✅ |
| Swiss-Knife (modified) | ✅ | ✅ | ❌ |
| TREAD |  |  |  |
| - Asymmetric (V1) original | ❌ | ❌ | ✅ |
| - Asymmetric (V1) fixed | ✅ | ❌ | ✅ |
| - Asymmetric (V2) original| ❌ | ❌ | ✅ |
| - Asymmetric (V2) fixed | ✅ | ❌ | ✅ |
| - Symmetric | ✅ | ❌ | ✅ |

<!--
======================================================================================================

Munilla					:	MFR [O] /	DHR [O] / 	TFR [X]		Time    ~01sec /	~01sec /	~01sec 
NXP						:	MFR [O] /	DHR [X] /	TFR [X]		Time	~01sec /	~10sec /	~01sec 
PAYSAFE					:	MFR [O] /	DHR [X] /	TFR [X]		Time	~01sec /	~30sec /	~01sec
SKI						:	MFR [O] /	DHR [O] /	TFR [O]		Time	~20sec /	~30min /	~20sec [DHR needs XOR weak] 
SPADE					:   MFR [X] /	DHR [X] /	TFR [O]		Time	~10sec /	~80min /	~15sec 
SPADE Fixed				:   MFR [O] /	DHR [X] /	TFR [O]		Time	~03sec /	~85min /	~05sec 
Swiss-Knife				:   MFR [O] /	DHR [O] /	TFR [O]		Time	~10sec /	~05sec /	~10sec 
Swiss-Knife Modified	:   MFR [O] /	DHR [O] /	TFR [X]		Time	~15sec /	~10sec /	~15sec 
TREAD_PK_V1				:	MFR [X] /	DHR [X] /	TFR [O]		Time	~03sec /	~01sec /	~03sec 
TREAD_PK_V1 Fixed		:	MFR [O] /	DHR [X] /	TFR [O]		Time	~01sec /	~01sec /	~01sec 
TREAD_PK_V2				:	MFR [X] /	DHR [X] /	TFR [O]		Time	~03sec /	~03sec /	~03sec 
TREAD_PK_V2 Fixed 		:	MFR [O] /	DHR [X] /	TFR [O]		Time	~01sec /	~04sec /	~01sec 
TREAD_SK				:	MFR [O] /	DHR [X] /	TFR [O]		Time	~01sec /	~02sec /	~01sec 
====================================================================================================== -->

Tests run using Cygwin on a laptop, with an Intel(R) Core(TM) i7-7700HQ CPU (2.80GHz, 2.80Ghz) processor, 
16Go RAM, and running Windows 10 - 64bits.

Concerning out of scope issues:
- Brands & Chaum and MAD(OneWay) protocols can not be tested for TFR in our methodology because the 
challenge sent to the Prover by the Verifier is not a fresh nonce that does not appear in previous 
messages. Indeed, the challenge is committed at the beginning of the protocol.

Concerning implementations:
- We would like to remind that, in ProVerif, we only model "honest" execution since dishonest executions
are performed directly by the adversary. (This is possible since our processes are executable.) This is
the reason why we only list the honest processes in our ProVerif files.

- Some protocols may have an "extended" version of "Weak XOR". This is because we wanted to add as much
power as possible to the attacker with respect to ProVerif capabilities. Therefore, we only maintained 
the strongest definitions for protocols that could "handle" it, i.e. without leading to non-termination
(in a reasonnable way).

## Modified version of ProVerif
As explained in the report, we must restrain the underlying attacker of ProVerif
when considering topologies without dishonest agents close to the verifier
(the non-modified ProVerif can still be used to verify Mafia fraud attacks). 
Indeed, the attacker model behind ProVerif allows him to interact with any participants, 
even those that are far away.
Therefore in the modified Proverif version, we prevent the attacker from forging 
new messages during Phase 1. He can only forward messages or send messages already 
forgeable in Phase 0. 
As presented in the report we need a modified version of the ProVerif tool to perform 
all the analyses.

#### Installation
The modified version of the ProVerif tool vuilds on ProVerif v1.97 and its sources are available 
in the corresponding repository. 

Execute **./build** to compile it. 

#### How to use?
After the compilation two executable files are generated: 
- proverif: it corresponds to the **non-modified** ProVerif tool. Use it to verify **Mafia and Terrorist fraud** attacks.
- proverifWeakAtt: it corresponds to the **modified** version of the ProVerif tool. Use it to verify **Distance Hijacking** attacks.