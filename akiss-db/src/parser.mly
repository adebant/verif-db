%{

(****************************************************************************)
(* Akiss                                                                    *)
(* Copyright (C) 2011-2014 Baelde, Ciobaca, Delaune, Kremer                 *)
(*                                                                          *)
(* This program is free software; you can redistribute it and/or modify     *)
(* it under the terms of the GNU General Public License as published by     *)
(* the Free Software Foundation; either version 2 of the License, or        *)
(* (at your option) any later version.                                      *)
(*                                                                          *)
(* This program is distributed in the hope that it will be useful,          *)
(* but WITHOUT ANY WARRANTY; without even the implied warranty of           *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *)
(* GNU General Public License for more details.                             *)
(*                                                                          *)
(* You should have received a copy of the GNU General Public License along  *)
(* with this program; if not, write to the Free Software Foundation, Inc.,  *)
(* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.              *)
(****************************************************************************)

open Ast

%}

%token <string> Identifier
%token <int> Int
%token <float> Float
%token XOR AC
%token Symbols PrivSymbols Private Var Rewrite EvRewrite Channels EvChannels Let TimeVar
%token PrivChannels
%token LeftP RightP LeftB RightB Vert
%token Arrow Equals Inequals Dot Slash Comma Semicolon Less Aff
%token Out In And Zero Plus InG
%token Not Equivalent Square EvSquare Variants Unifiers Normalize Incft Incct
%token Print PrintTraces
%token InnerSequence InnerInterleave InnerChoice InnerPhase
%token If Then Else
%token EOF
%token Reach
%token Topology InitTimes
%token Honest Dishonest
%token Minus

%left In
%left InnerPhase
%left InnerSequence
%left InnerInterleave
%left InnerChoice
%right Dot

%start main

%type <Ast.cmd list> main

%%

main:
 | commandlist EOF { $1 }

     commandlist:
 | { [] }
 | command Semicolon commandlist { $1 :: $3 }
     
     command:
 | XOR { SetXOR }
 | AC { SetAC }
 | Symbols symbollist { DeclSymbols $2 }
 | PrivSymbols symbollist { DeclPrivSymbols $2 }
 | Private namelist { DeclPrivate $2 }
 | Channels namelist { DeclChannels $2 }
 | EvChannels namelist { DeclEvChannels $2 }
 | PrivChannels namelist { DeclPrivChannels $2 }
 | Var namelist { DeclVar $2 }
 | Rewrite term Arrow term { DeclRewrite ($2, $4) }
 | EvRewrite term Arrow term { DeclEvRewrite ($2, $4) }
 | Identifier Equals process { DeclProcess ($1, $3) }
 | Print Identifier { QueryPrint $2 }
 | PrintTraces identifierList { QueryPrintTraces $2 }
 | Variants term { QueryVariants $2 }
 | Unifiers term term { QueryUnifiers ($2, $3) }
 | Normalize term { QueryNormalize $2 }     
 | negatable { QueryNegatable (true, $1) }
 | Not negatable { QueryNegatable (false, $2) }
 | TimeVar namelist { DeclVarTime $2 }
 | Reach Identifier In Identifier { QueryReach ($2, $4) }
 | Topology Equals locations { DeclLocations $3 }
 | InitTimes Equals initTime { DeclInitTimes $3 }

     negatable:
 | Equivalent identifierList And identifierList { NegEquivalent ($2, $4) }
 | Square identifierList And identifierList { NegSquare ($2, $4) }
 | EvSquare identifierList And identifierList { NegEvSquare ($2, $4) }
 | Incft identifierList In identifierList { NegIncFt ($2, $4) }
 | Incct identifierList In identifierList { NegIncCt ($2, $4) }

     identifierList:
 | { [] }
 | neidentifierList { $1 }

     neidentifierList:
 | Identifier { [$1] }
 | Identifier Comma neidentifierList { $1 :: $3 }

     process:
 | Zero { TempEmpty }
 | LeftP Identifier Comma Identifier Comma action RightP { TempAction($2,$4,$6) }
 | LeftP Identifier Comma Identifier Comma action RightP Dot process { TempSequence(TempAction($2,$4,$6), $9) }
 | LeftP Identifier Comma action RightP { TempAction($2,"!none!",$4) }
 | LeftP Identifier Comma action RightP Dot process { TempSequence(TempAction($2,"!none!",$4), $7) }
 | process InnerSequence process { TempSequence($1, $3) }
 | process InnerInterleave process { TempInterleave($1, $3) }
 | process InnerChoice process { TempChoice($1, $3) }
 | process InnerPhase process { TempPhase($1, $3) }
 | LeftP process RightP { $2 }
 | Identifier { TempProcessRef($1) }
	      
     action:
 | In LeftP Identifier RightP { TempActionIn($3) }
 | Out LeftP term RightP { TempActionOut($3) }
 | LeftB term Equals term RightB { TempActionTest ($2, $4) }
 | LeftB Vert Identifier Aff term Vert RightB { TempActionAff($3, $5) }
 | LeftB Vert expr timeSymb term Vert RightB { TempActionTestTime($4,$3,$5) }

     expr:
 | term Minus term { [$1; $3] }
 | term { [$1] }	
	 
     term:
 | Identifier { TempTermCons ($1, []) }
 | Float { TempTermCons (string_of_float $1, []) }
 | Int { TempTermCons (string_of_float (float_of_int $1), []) }
 | Identifier LeftP termlist RightP { TempTermCons ($1, $3) }
 | term Plus term { TempTermCons ("plus", [$1;$3]) }
 | LeftP term Plus term RightP { TempTermCons ("plus", [$2;$4]) }
 | Zero {TempTermCons ("zero",[]) } 

     termlist:
 | { [] }
 | netermlist { $1 }

     netermlist:
 | term { [ $1 ] }
 | term Comma netermlist { $1 :: $3 }

     symbollist:
 | { [] }
 | nesymbollist { $1 }

     nesymbollist:
 | symbol { [ $1 ] }
 | symbol Comma nesymbollist { $1 :: $3 }

     symbol:
 | Identifier Slash Int { ($1, $3) }
 | Identifier Slash Zero { ($1, 0) }

     namelist:
 | { [] }
 | nenamelist { $1 }

     nenamelist:
 | name { [ $1 ] }
 | name Comma nenamelist { $1 :: $3 }

     name:
 | Identifier { $1 }

     locations:
 | { [] }
 | location  { [ $1 ] }
 | location Comma locations { $1 :: $3 }

     location:
 | LeftB Identifier Comma LeftP zeroInt Comma zeroInt Comma zeroInt  RightP Comma status  RightB { ($2, (float_of_int $5, float_of_int $7, float_of_int $9), $12) }
 | LeftB Identifier Comma LeftP Float Comma zeroInt Comma zeroInt  RightP Comma status  RightB { ($2, ($5, float_of_int $7, float_of_int $9), $12) }
 | LeftB Identifier Comma LeftP zeroInt Comma Float Comma zeroInt  RightP Comma status  RightB { ($2, (float_of_int $5, $7, float_of_int $9), $12) }
 | LeftB Identifier Comma LeftP zeroInt Comma zeroInt Comma Float  RightP Comma status  RightB { ($2, (float_of_int $5, float_of_int $7, $9), $12) }
 | LeftB Identifier Comma LeftP Float Comma Float Comma zeroInt  RightP Comma status  RightB { ($2, ($5, $7, float_of_int $9), $12) }
 | LeftB Identifier Comma LeftP Float Comma zeroInt Comma Float  RightP Comma status  RightB { ($2, ($5, float_of_int $7, $9), $12) }
 | LeftB Identifier Comma LeftP zeroInt Comma Float Comma Float  RightP Comma status  RightB { ($2, (float_of_int $5, $7, $9), $12) }
 | LeftB Identifier Comma LeftP Float Comma Float Comma Float  RightP Comma status  RightB { ($2, ($5, $7, $9), $12) }

      zeroInt:
 | Zero { 0 }
 | Int { $1 }
	 
      status:
 | Honest { "honest" }
 | Dishonest { "dishonest" }

      initTime:
 | zeroInt { float_of_int $1 }
 | Float { $1 }

      timeSymb:
 | Equals { "=" }
 | Less { "<" }
 | Less Equals { "<=" }
	 
%%
