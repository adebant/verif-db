
(*
copyright (c) 2013-2014, guillaume bury
all rights reserved.

redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.  redistributions in binary
form must reproduce the above copyright notice, this list of conditions and the
following disclaimer in the documentation and/or other materials provided with
the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*)

open Format
open Term
open Util
open Process
open Theory
open Process_execution
       
module S = Simplex.Make(struct type t = varName let compare = Pervasives.compare end)
;;

let max_depth = 5
let max_rand = 100
let bound_range = 30

let svar x = sprintf "v%d" x

let print_var fmt x = fprintf fmt "%s" (x)

let print_short fmt = function
    | S.Solution _ -> fprintf fmt "SAT"
    | S.Unsatisfiable _ -> fprintf fmt "UNSAT"

let print_res f fmt = function
    | S.Solution l ->
            fprintf fmt "Sol:@\n%a@." (fun fmt -> List.iter (fun (x, v) -> fprintf fmt "%a : %s@\n" print_var x (Q.to_string v))) l
    | S.Unsatisfiable c -> fprintf fmt "UNSAT:@\n%a@." f c

let print_unsat fmt (x, l) =
    fprintf fmt "%a =@ @[<hov 2>%a@]" print_var x (fun fmt l ->
        if l = [] then fprintf fmt "()" else List.iter (fun (c, x) -> fprintf fmt "%s * %a +@ " (Q.to_string c) print_var x) l) l

let print_abs fmt l =
    let aux (x, (e, k)) =
        fprintf fmt "v%d == %a%s@." x
        (fun fmt -> List.iter (fun (c, x) -> fprintf fmt "%s * v%d + " (Q.to_string c) x)) e
        (Q.to_string k)
    in
    List.iter aux l

let rec print_branch n fmt b =
    if n > max_depth then
        fprintf fmt "..."
    else match !b with
    | None -> raise Exit
    | Some (S.Branch (x, v, c1, c2)) ->
            fprintf fmt "@[<hov 6>%a <= %s :@\n%a@]@\n@[<hov 6>%a >= %s :@\n%a@]"
            print_var x (Z.to_string v) (print_branch (Pervasives.(+) n 1)) c1
            print_var x (Z.to_string (Z.succ v)) (print_branch (Pervasives.(+) n 1)) c2
    | Some (S.Explanation c) ->
            fprintf fmt "Unsat:@ %a" print_unsat c

let print_ksol = print_res print_unsat
let print_nsol = print_res (print_branch 0)


(***
clocks: it is the list of all the global time variables ; 
        initially it constains only one variable called 'xt0' 
        but when the time elapses we add variables such that 
        "xt1 >= xt0 + delta"
personnal_clocks: it is an association list such that "List.assoc clock personnal_clock"
                  return the global_time variable of the last reset of this clock
outputs: it is an association list linking each output to the time variable 
         corresponding to the date of the output in the system of constraints
list_const_set: it is the list of all the contraints sets already computed
***)
let global_time = ref "t0"
(*let personnal_clocks = ref []*)
let nb_outputs = ref 0
let outputs = ref []		  
let list_const_set = ref []
(*let init_time = ref 0*)
			 
(* if there are no dishonest agents, traces with complex recipes 
(i.e. different from a variable 'w') cannot be played *) 		 
exception CannotBePlayed 
	    
let rec print_outputs = function
  | [] -> ""
  | (x,_)::q -> (string_of_int x) ^ "  " ^ (print_outputs q)
	    
let rec frame_var_in_term t =
  match t with
  | Fun(w, []) when startswith w "w" -> [ ( int_of_string (String.sub w 1 ((String.length w) -1)))]
  | Fun(w, []) -> []
  | Var(_) -> []
  | VarTime(_) -> []
  | Fun(w, ls) ->
     List.fold_left (fun x y -> List.append x (frame_var_in_term y)) [] ls  

let add_elapsing_time z =
  let delta = fresh_variable () in
  let new_var = 
    if not (String.equal z "!none!") then z
    else fresh_variable () in
  List.iter (fun s ->
	     s := S.add_eq !s (new_var, [Q.of_int 1, (!global_time); Q.of_int 1, delta]);
	     s := S.add_bounds !s (delta, Q.of_int 0, Q.inf);
	    ) !list_const_set ;
  global_time := new_var
		    
let add_input_const id r =
  try
    match r with
    | Fun(w, []) when startswith w "w" ->
       let var = ( int_of_string (String.sub w 1 ( (String.length w) -1))) in
       let (id_sender, w_var) = List.assoc var !outputs in
       let current_time = !global_time in
       let (x1,y1,z1) = List.assoc id !locations in
       let (x2,y2,z2) = List.assoc id_sender !locations in
       let (dist:float) = dist (x1,y1,z1) (x2,y2,z2) in
       let temp = fresh_variable () in 
       List.iter (fun s -> 
		  s := S.add_eq !s (temp, [Q.of_int 1, current_time; Q.minus_one, w_var]);
		  s := S.add_bounds !s (temp, Q.of_float dist, Q.inf)
		 ) !list_const_set ;
    | Fun(w, []) when startswith w "!n!" -> ()
    | _ ->
       if List.length !corruptedId = 0 then raise CannotBePlayed (* only corrupted agents can construct complex recipes *)
       else
	 begin
	   let (new_sets:S.t ref list ref) = ref [] in
	   List.iter (fun s' ->
		      List.iter ( fun cId ->
				  let s = ref (S.copy !s') in
				  let frame_vars_in_r = frame_var_in_term r in
				  List.iter (fun x ->
					     let (id_sender, w_var) = List.assoc x !outputs in
					     let current_time = !global_time in
					     let locReceiver = List.assoc id !locations in
					     let locSender = List.assoc id_sender !locations in
					     let locMaker = List.assoc cId !locations in
					     let (distSM:float) = dist locSender locMaker in
					     let (distMR:float) = dist locMaker locReceiver in
					     let temp = fresh_variable () in
					     s := S.add_eq !s (temp, [Q.of_int 1, current_time; Q.minus_one, w_var]);
					     s := S.add_bounds !s (temp, (Q.of_float (distSM +. distMR)), Q.inf)
					    ) frame_vars_in_r;
				  new_sets := s :: !new_sets;
			    ) !corruptedId;
		     )!list_const_set;
	   list_const_set := !new_sets
	 end 
			  
  with 
  | Not_found -> Printf.printf "\n  id = %s\n\n recipe = %s \n\n" id (show_term r) ;failwith "Not_found in add_input_const"
			  
let time_var x =
  let x = R.normalize x Theory.rewrite_rules in
  (*Printf.printf "\ntime_var with x=%s and fresh_time_var=" (show_term x);
  List.iter (fun (x,_) -> Printf.printf " %s," x) !fresh_time_var;
  Printf.printf "\n";*)
  try
  match x  with
  | Fun (t, []) when startswith t "!n!" -> (t,false)
     (*if List.mem_assoc t !fresh_time_var then
       (t,List.assoc t !fresh_time_var)
     else
       let n = List.length !fresh_time_var in
       fresh_time_var := (t,n +1) :: !fresh_time_var;
       (t,n+1)	*)						
  | Fun (t, []) ->
     begin
       match float_of_string_opt t with
       | None -> ("", false)
       | Some v -> (t,true)
     end
  | VarTime t -> (t,false)
  | _ -> Printf.printf "\n\n %s \n" (show_term x); failwith "1"
  with Not_found -> failwith "Not_found in time_var"

let add_timing_constraint op x y =
  let v,b = time_var y in
  try
    match x with
    | [] -> assert false;
    | [z1] ->
       let v1,b1 = time_var z1 in
       if String.equal "=" op then
	 if b && b1 then (* I am testing t1 == t2 *)
	   if float_of_string v1 != float_of_string v then raise CannotBePlayed
	   else ()
	 else if b then (
	   List.iter (fun s' ->
		      s' := S.add_bounds !s' (v1, Q.of_float @@ float_of_string v, Q.of_float @@ float_of_string v)
		     ) !list_const_set
	 )
	 else if b1 then (
	   List.iter (fun s' ->
		      s' := S.add_bounds !s' (v, Q.of_float @@ float_of_string v1, Q.of_float @@ float_of_string v1)
		     ) !list_const_set
	 )
	 else
	   List.iter (fun s' ->
		      s' := S.add_eq !s' (v, [Q.of_int 1, v1] )
		     ) !list_const_set
		     
       else if String.equal "<" op then
	 if b && b1 then
	   if float_of_string v1 >= float_of_string v then raise CannotBePlayed
	   else ()
	 else if b then (
	   List.iter (fun s' ->
		      s' := S.add_bounds ~strict_upper:true !s' (v1, Q.minus_inf, Q.of_float @@ float_of_string v)
		     ) !list_const_set
	 )
	 else if b1 then (
	   List.iter (fun s' ->
		      s' := S.add_bounds ~strict_upper:true !s' (v, Q.of_float @@ float_of_string v1, Q.inf)
		     ) !list_const_set
	 )
	 else
	   List.iter (fun s' ->
		      let temp = fresh_variable () in
		      s' := S.add_eq !s' (temp, [Q.of_int 1, v1; Q.minus_one, v ]);
		      s' := S.add_bounds !s' (temp, Q.minus_inf, Q.of_int 0)
		     ) !list_const_set
		     
       else if String.equal "<=" op then
	 if b && b1 then 
	   if float_of_string v1 > float_of_string v then raise CannotBePlayed
	   else ()
	 else if b then (
	   List.iter (fun s' ->
		      s' := S.add_bounds !s' (v1, Q.minus_inf, Q.of_float @@ float_of_string v)
		     ) !list_const_set
	 )
	 else
	   List.iter (fun s' ->
		      let temp = fresh_variable () in
		      s' := S.add_eq !s' (temp, [Q.of_int 1, v1; Q.minus_one, v ]);
		      s' := S.add_bounds !s' (temp, Q.minus_inf, Q.of_int 0)
		     ) !list_const_set
		    
       else failwith "not supported operator"
    | [z1;z2] ->
       let v1,b1 = time_var z1 in
       let v2,b2 = time_var z2 in
       (*Printf.printf "\n\n %s - %s < %s" vZ1 vZ2 v;*)
       if String.equal "=" op then
	 if b && b1 && b2 then
	   if (float_of_string v1) -. (float_of_string v2) != (float_of_string v) then raise CannotBePlayed
	   else ()
	 else if b && b1 then (
	   List.iter (fun s' ->
		      s' := S.add_bounds !s' (v2,
					     Q.of_float ((float_of_string v1) -. (float_of_string v)),
					     Q.of_float ((float_of_string v1) -. (float_of_string v))
					    )
		     ) !list_const_set
	 )
	 else if b && b2 then (
	   List.iter (fun s' ->
		      s' := S.add_bounds !s' (v1,
					     Q.of_float ((float_of_string v2) +. (float_of_string v)),
					     Q.of_float ((float_of_string v2) +. (float_of_string v))
					    )
		     ) !list_const_set
	 )
	 else if b1 && b2 then (
	   List.iter (fun s' ->
		      s' := S.add_bounds !s' (v,
					     Q.of_float ((float_of_string v1) -. (float_of_string v2)),
					     Q.of_float ((float_of_string v1) -. (float_of_string v2))
					    )
		     ) !list_const_set
	 )
	 else if b then (
	   List.iter (fun s' ->
		      let temp = fresh_variable () in
		      s' := S.add_eq !s' (temp, [Q.of_int 1, v1; Q.minus_one, v2 ]);
		      s' := S.add_bounds !s' (temp,
					     Q.of_float (float_of_string v),
					     Q.of_float (float_of_string v)
					    )
		     ) !list_const_set
	 )
	 else if b1 then (
	   List.iter (fun s' ->
		      let temp = fresh_variable () in
		      s' := S.add_eq !s' (temp, [Q.of_int 1, v; Q.of_int 1, v2 ]);
		      s' := S.add_bounds !s' (temp,
					     Q.of_float (float_of_string v1),
					     Q.of_float (float_of_string v1)
					    )
		     ) !list_const_set
	 )
	 else if b2 then (
	   List.iter (fun s' ->
		      let temp = fresh_variable () in
		      s' := S.add_eq !s' (temp, [Q.of_int 1, v1; Q.of_int 1, v ]);
		      s' := S.add_bounds !s' (temp,
					     Q.of_float (float_of_string v2),
					     Q.of_float (float_of_string v2)
					    )
		     ) !list_const_set
	 )
	 else 
	   (
	     List.iter (fun s' ->
			s' := S.add_eq !s' (v, [Q.of_int 1, v1; Q.minus_one, v2 ]);
		     ) !list_const_set
	   )
	 
       else if String.equal "<" op then
	if b && b1 && b2 then
	   if (float_of_string v1) -. (float_of_string v2) >= (float_of_string v) then raise CannotBePlayed
	   else ()
	 else if b && b1 then (
	   List.iter (fun s' ->
		      s' := S.add_bounds ~strict_upper:true !s' (v2,
					     Q.of_float ((float_of_string v1) -. (float_of_string v)),
					     Q.inf
					    )
		     ) !list_const_set
	 )
	 else if b && b2 then (
	   List.iter (fun s' ->
		      s' := S.add_bounds ~strict_upper:true !s' (v1,
					     Q.minus_inf,
					     Q.of_float ((float_of_string v2) +. (float_of_string v))
					    )
		     ) !list_const_set
	 )
	 else if b1 && b2 then (
	   List.iter (fun s' ->
		      s' := S.add_bounds ~strict_upper:true !s' (v,
					     Q.of_float ((float_of_string v1) -. (float_of_string v2)),
					     Q.inf
					    )
		     ) !list_const_set
	)
	else if b then (
	  List.iter (fun s' ->
		     let temp = fresh_variable () in
			s' := S.add_eq !s' (temp, [Q.of_int 1, v1; Q.minus_one, v2]);
			s' := S.add_bounds ~strict_upper:true !s' (temp, Q.minus_inf, Q.of_float @@ float_of_string v)
		    ) !list_const_set
	)
	else if b1 then (
	  List.iter (fun s' ->
		     let temp = fresh_variable () in
			s' := S.add_eq !s' (temp, [Q.of_int 1, v; Q.of_int 1, v2]);
			s' := S.add_bounds ~strict_upper:true !s' (temp, Q.of_float @@ float_of_string v1, Q.inf)
		    ) !list_const_set
	)
	else if b2 then (
	  List.iter (fun s' ->
		     let temp = fresh_variable () in
			s' := S.add_eq !s' (temp, [Q.of_int 1, v1; Q.minus_one, v]);
			s' := S.add_bounds ~strict_upper:true !s' (temp, Q.minus_inf, Q.of_float @@ float_of_string v2)
		    ) !list_const_set
	)
	else 
	   (
	     List.iter (fun s' ->
			let temp = fresh_variable () in
			s' := S.add_eq !s' (temp, [Q.of_int 1, v1; Q.minus_one, v2; Q.minus_one, v]);
			s' := S.add_bounds ~strict_upper:true !s' (temp, Q.minus_inf, Q.of_int 0)
		     ) !list_const_set
	   )
       else if String.equal "<=" op then
	 if b && b1 && b2 then
	   if (float_of_string v1) -. (float_of_string v2) > (float_of_string v) then raise CannotBePlayed
	   else ()
	 else if b && b1 then (
	   List.iter (fun s' ->
		      s' := S.add_bounds !s' (v2,
					     Q.of_float ((float_of_string v1) -. (float_of_string v)),
					     Q.inf
					    )
		     ) !list_const_set
	 )
	 else if b && b2 then (
	   List.iter (fun s' ->
		      s' := S.add_bounds !s' (v1,
					     Q.minus_inf,
					     Q.of_float ((float_of_string v2) +. (float_of_string v))
					    )
		     ) !list_const_set
	 )
	 else if b1 && b2 then (
	   List.iter (fun s' ->
		      s' := S.add_bounds !s' (v,
					     Q.of_float ((float_of_string v1) -. (float_of_string v2)),
					     Q.inf
					    )
		     ) !list_const_set
	)
	else if b then (
	  List.iter (fun s' ->
		     let temp = fresh_variable () in
			s' := S.add_eq !s' (temp, [Q.of_int 1, v1; Q.minus_one, v2]);
			s' := S.add_bounds !s' (temp, Q.minus_inf, Q.of_float @@ float_of_string v)
		    ) !list_const_set
	)
	else if b1 then (
	  List.iter (fun s' ->
		     let temp = fresh_variable () in
			s' := S.add_eq !s' (temp, [Q.of_int 1, v; Q.of_int 1, v2]);
			s' := S.add_bounds !s' (temp, Q.of_float @@ float_of_string v1, Q.inf)
		    ) !list_const_set
	)
	else if b2 then (
	  List.iter (fun s' ->
		     let temp = fresh_variable () in
			s' := S.add_eq !s' (temp, [Q.of_int 1, v1; Q.minus_one, v]);
			s' := S.add_bounds !s' (temp, Q.minus_inf, Q.of_float @@ float_of_string v2)
		    ) !list_const_set
	)
	else 
	   (
	     List.iter (fun s' ->
			let temp = fresh_variable () in
			s' := S.add_eq !s' (temp, [Q.of_int 1, v1; Q.minus_one, v2; Q.minus_one, v]);
			s' := S.add_bounds !s' (temp, Q.minus_inf, Q.of_int 0)
		     ) !list_const_set
	   )
       else failwith "not supported operator"
    | _ -> failwith "not supported expression"
  with
    Not_found -> failwith "free time variable"
			  
let rec make_constraints_set trace test frame =
  match trace, test with
  | (_, Fun("world", [Fun("!begin!", []); ir])) -> make_constraints_set trace ir frame
  | (_, Fun("check_run", tr)) ->  make_constraints_set trace (Fun("world",(Fun("!begin!", []))::tr)) frame
  | (NullTrace, Fun("empty", [])) -> (*!list_const_set*) ()
  | (Trace(Input(id, z, x), pr), Fun("world", [Fun("!in!", [r]); ir])) ->
     add_elapsing_time z;
     add_input_const id r;
     let new_pr = apply_subst_tr pr [x, apply_frame r frame] in
     make_constraints_set new_pr ir frame

  | (Trace(Test(id, z, x, y), pr), Fun("world", [Fun("!test!", _); ir])) -> 
     add_elapsing_time z;
     make_constraints_set pr ir	frame

  | (Trace(Output(id, z, x), pr), Fun("world", [Fun("!out!", _); ir])) ->
     add_elapsing_time z;
     outputs := (!nb_outputs, (id, !global_time))::(!outputs);
     nb_outputs := 1 + !nb_outputs;
     make_constraints_set pr ir	(List.append frame [x])

  | (Trace(TestTime(id,z,op, x, y), pr), Fun("world", [Fun("!testTime!", _); ir])) ->
     add_elapsing_time z;
     add_timing_constraint op x y;
     make_constraints_set pr ir frame

  | (Trace(Let(id,z,x, t), pr), Fun("world", [Fun("!let!", _); ir])) ->
     add_elapsing_time z;
     begin
       match R.normalize t Theory.rewrite_rules with
       | Fun (s, []) as s' when startswith s "!n!" ->
	  List.iter (fun s' ->
			s' := S.add_eq !s' (s, [Q.of_int 1, s]);
		     ) !list_const_set;
	  make_constraints_set (apply_subst_tr pr [x,s']) ir frame
       | Fun (s, []) as s'->
	  let _,b = time_var s' in
	  if b then (
	    (*List.iter (fun matrix ->
			matrix := S.add_eq !matrix (s, [Q.of_int 1, s]);
		      ) !list_const_set;*)
	    make_constraints_set (apply_subst_tr pr [x,s']) ir frame
	  )
	  else raise CannotBePlayed
       | _ -> raise CannotBePlayed
     end
			  
  | _ ->
     Printf.printf "\n\n Trace = %s \n Test = %s \n\n" (show_trace trace) (show_term test);
     failwith "make_constraints_set: not compatible trace and test"
			  
			  
exception Attack of trace * term
exception End
		  
let verify_constraints_simplexe trace tests =
  try 
    List.iter (
	fun x ->
	try 
	  (*counter := Util.add 1 !counter;
	Printf	    .printf "Tests : %d/%d" !counter nb_tot; *)
	  global_time := "t0";
	  (*personnal_clocks := [];*)
	  nb_outputs := 0;
	  outputs := [];
	  let s = ref S.empty in
	  s := S.add_eq !s ("t0", [Q.of_int 1, "t0"]);
	  s := S.add_bounds !s ("t0", Q.of_float (List.assoc "init" !init_clock_values), Q.of_float (List.assoc "init" !init_clock_values));
	  list_const_set := [s];
	  make_constraints_set trace x [];
	  
	  (*Printf.printf "\n test = %s" (show_term x);*)
	  List.iter (fun s ->
		     let res = S.ksolve !s in
		     match res with
		     | S.Solution l -> raise (Attack (trace,x))
		     | _ -> ()
		    ) !list_const_set
	with CannotBePlayed -> ()
      ) tests
  with
  (*| Attack(tr,test) -> Printf.printf "\n\nATTACK: \n  %s \n   %s \n\n" (show_trace trace) (show_term test);
		  raise End*)
  | Not_found -> failwith "Not_found in verify_constraints"
		  
