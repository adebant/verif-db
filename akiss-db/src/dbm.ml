open Format
open Term
open Util
open Process
open Theory
open Process_execution


type floatExt = Inf | Some of float

let print_matrix matrix =
  let n = Array.length matrix in
  for i = 0 to n-1 do
    for j = 0 to n-1 do
      if j = 0 then Printf.printf "( ";
      let (v,b) = matrix.(i).(j) in
      (
	match v with
	| Inf -> (*Printf.printf "[inf,%b]"  b;*)
		 Printf.printf "inf";
	| Some v -> (*Printf.printf "[%d,%b]" (int_of_float v) b;*)
	   Printf.printf " %d " (int_of_float v);
      )
      ;
      if j != n-1 then Printf.printf ", ";
      if j = n-1 then Printf.printf " )";
    done;
    Printf.printf "\n";
  done

let rec frame_var_in_term t =
  match t with
  | Fun(w, []) when startswith w "w" -> [int_of_string (String.sub w 1 ((String.length w)- 1))]
  | Fun(w, []) -> []
  | Var(_) -> []
  | VarTime(_) -> []
  | Fun(w, ls) ->
     List.fold_left (fun x y -> List.append x (frame_var_in_term y)) [] ls

let outputs = ref []
let current_variable = ref 1
let global_time = ref 1
let list_const_set = ref []
let (personnal_clocks) = ref []
let nb_outputs = ref 0
(*let (fresh_time_var: (Term.funName * int) list ref) = ref []*)

exception CannotBePlayed 
			   
let add_elapsing_time () =
  List.iter (fun matrix ->
	     if !current_variable >= Array.length matrix then failwith "matrix too small";
	     matrix.(!global_time).(!current_variable) <- (Some 0., false);
	    ) !list_const_set ;
  global_time := !current_variable 

let copy_matrix m = 
	let m' = Array.make_matrix (Array.length m) (Array.length m) (Some 0., false) in
	for i=0 to Array.length m -1 do
	  m'.(i) <- Array.copy m.(i)
	done;
	m'
    
let  add_input_const id r =
  try
    match r with
    | Fun(w, []) when startswith w "w" ->
       let var = (int_of_string (String.sub w 1 ((String.length w) - 1))) in
       let (id_sender, w_var) = List.assoc var !outputs in
       let current_time = !global_time in
       let (x1,y1,z1) = List.assoc id !locations in
       let (x2,y2,z2) = List.assoc id_sender !locations in
       let (dist:float) = dist (x1,y1,z1) (x2,y2,z2) in
       List.iter (fun matrix ->
		  matrix.(w_var).(current_time) <- (Some (0. -. dist), false)
		 ) !list_const_set ;
    | Fun(name, []) when startswith name "!n!" -> () (* such a constant is always available *)
    | _ ->
       if List.length !corruptedId = 0 then raise CannotBePlayed (* only corrupted agents can construct complex recipes *)
       else
	 begin
	   let (new_sets:(floatExt * bool) array array list ref) = ref [] in
	   List.iter (fun matrix ->
		      List.iter ( fun cId ->
				  let s = copy_matrix matrix in
				  let frame_vars_in_r = frame_var_in_term r in
				  List.iter (fun x ->
					     let (id_sender, w_var) = List.assoc x !outputs in
					     let current_time = !global_time in
					     let locReceiver = List.assoc id !locations in
					     let locSender = List.assoc id_sender !locations in
					     let locMaker = List.assoc cId !locations in
					     let (distSM:float) = dist locSender locMaker in
					     let (distMR:float) = dist locMaker locReceiver in
					     s.(w_var).(current_time) <- (Some (0. -. distSM -. distMR), false)
					    ) frame_vars_in_r;
				  new_sets := s :: !new_sets;
			    ) !corruptedId;
		     )!list_const_set;
	   list_const_set := !new_sets
	 end 
			  
  with 
  | Not_found -> failwith "Not_found in add_input_const"

let time_var x =
  let x = R.normalize x Theory.rewrite_rules in
  (*Printf.printf "\ntime_var with x=%s and fresh_time_var=" (show_term x);
  List.iter (fun (x,_) -> Printf.printf " %s," x) !fresh_time_var;
  Printf.printf "\n";*)
  try
  match x  with
  | Fun (t, []) when startswith t "!n!" -> (t,false)
     (*if List.mem_assoc t !fresh_time_var then
       (t,List.assoc t !fresh_time_var)
     else
       let n = List.length !fresh_time_var in
       fresh_time_var := (t,n +1) :: !fresh_time_var;
       (t,n+1)	*)						
  | Fun (t, []) ->
     begin
       match float_of_string_opt t with
       | None -> ("", false)
       | Some v -> (t,true)
     end
  | VarTime t -> (t,false)
  | _ -> Printf.printf "\n\n %s \n" (show_term x); failwith "1"
  with Not_found -> failwith "Not_found in time_var"

exception CannotResolveWithDBM
			     
let add_timing_constraint op x y =
  let v,b = time_var y in
  try
    match x with
    | [] -> assert false;
    
    | [z1] ->
       let v1,b1 = time_var z1 in
       if String.equal "=" op then
	 if b && b1 then (* I am testing t1 == t2 *)
	   if float_of_string v1 != float_of_string v then raise CannotBePlayed
	   else ()
	 else if b then (
	   List.iter (fun matrix ->
		    matrix.(List.assoc v1 !personnal_clocks).(0) <- (Some (float_of_string v),false);
		    matrix.(0).(List.assoc v1 !personnal_clocks) <- (Some (0. -.(float_of_string v)), false);
		   ) !list_const_set
	 )
	 else if b1 then (
	   List.iter (fun matrix ->
		    matrix.(List.assoc v !personnal_clocks).(0) <- (Some (float_of_string v1),false);
		    matrix.(0).(List.assoc v !personnal_clocks) <- (Some (0. -.(float_of_string v1)), false);
		   ) !list_const_set
	 )
	 else
	   List.iter (fun matrix ->
		      matrix.(List.assoc v1 !personnal_clocks).(List.assoc v !personnal_clocks) <- (Some (0.),false);
		      matrix.(List.assoc v !personnal_clocks).(List.assoc v1 !personnal_clocks) <- (Some (0.),false);
		   ) !list_const_set
	     
       else if String.equal "<" op then
	 if b && b1 then
	   if float_of_string v1 >= float_of_string v then raise CannotBePlayed
	   else ()
	 else if b then (
	   List.iter (fun matrix ->
		    matrix.(List.assoc v1 !personnal_clocks).(0) <- (Some (float_of_string v),true);
		   ) !list_const_set
	 )
	 else if b1 then (
	   List.iter (fun matrix ->
		    matrix.(0).(List.assoc v !personnal_clocks) <- (Some (0. -. float_of_string v1),true);
		   ) !list_const_set
	 )
	 else
	   List.iter (fun matrix ->
		    matrix.(List.assoc v1 !personnal_clocks).(List.assoc v !personnal_clocks) <- (Some (0.),true);
		   ) !list_const_set
		     
       else if String.equal "<=" op then
	 if b && b1 then
	   if float_of_string v1 > float_of_string v then raise CannotBePlayed
	   else ()
	 else if b then (
	   List.iter (fun matrix ->
		    matrix.(List.assoc v1 !personnal_clocks).(0) <- (Some (float_of_string v),false);
		   ) !list_const_set
	 )
	 else if b1 then (
	   List.iter (fun matrix ->
		    matrix.(0).(List.assoc v !personnal_clocks) <- (Some (0. -. float_of_string v1),false);
		   ) !list_const_set
	 )
	 else
	   List.iter (fun matrix ->
		    matrix.(List.assoc v1 !personnal_clocks).(List.assoc v !personnal_clocks) <- (Some (0.),false);
		   ) !list_const_set
		    
       else failwith "not supported operator"	     


    | [z1;z2] ->
       let v1,b1 = time_var z1 in
       let v2,b2 = time_var z2 in
       (*Printf.printf "\n\n %s - %s < %s" vZ1 vZ2 v;*)
       if String.equal "=" op then
	 if b && b1 && b2 then
	   if (float_of_string v1) -. (float_of_string v2) != (float_of_string v) then raise CannotBePlayed
	   else ()
	 else if b && b1 then (
	   List.iter (fun matrix ->
		      matrix.(0).(List.assoc v2 !personnal_clocks) <- (Some (float_of_string v -. float_of_string v1),false);
		      matrix.(List.assoc v2 !personnal_clocks).(0) <- (Some (float_of_string v1 -. float_of_string v),false);
		   ) !list_const_set
	 )
	 else if b && b2 then (
	   List.iter (fun matrix ->
		      matrix.(List.assoc v1 !personnal_clocks).(0) <- (Some (float_of_string v +. float_of_string v2),false);
		      matrix.(0).(List.assoc v1 !personnal_clocks) <- (Some (0. -. float_of_string v -. float_of_string v2),false);
		   ) !list_const_set
	 )
	 else if b1 && b2 then (
	   List.iter (fun matrix ->
		      matrix.(List.assoc v !personnal_clocks).(0) <- (Some (float_of_string v1 -. float_of_string v2),false);
		      matrix.(0).(List.assoc v !personnal_clocks) <- (Some (float_of_string v2 -. float_of_string v1),false);
		   ) !list_const_set
	 )
	 else if b then (
	  List.iter (fun matrix ->
		      matrix.(List.assoc v1 !personnal_clocks).(List.assoc v2 !personnal_clocks) <- (Some (float_of_string v),false);
		      matrix.(List.assoc v2 !personnal_clocks).(List.assoc v1 !personnal_clocks) <- (Some (0. -. float_of_string v),false);
		   ) !list_const_set
	 )
	 else if b1 then (
	   raise CannotResolveWithDBM
	 )
	 else if b2 then (
	   List.iter (fun matrix ->
		      matrix.(List.assoc v1 !personnal_clocks).(List.assoc v !personnal_clocks) <- (Some (float_of_string v2),false);
		      matrix.(List.assoc v !personnal_clocks).(List.assoc v1 !personnal_clocks) <- (Some (0. -. float_of_string v2),false);
		   ) !list_const_set
	 )
	 else 
	   (
	     raise CannotResolveWithDBM
	   )
	 
       else if String.equal "<" op then
	 if b && b1 && b2 then
	   if (float_of_string v1) -. (float_of_string v2) >= (float_of_string v) then raise CannotBePlayed
	   else ()
	 else if b && b1 then (
	   List.iter (fun matrix ->
		      matrix.(0).(List.assoc v2 !personnal_clocks) <- (Some (float_of_string v -. float_of_string v1),true);
		   ) !list_const_set
	 )
	 else if b && b2 then (
	   List.iter (fun matrix ->
		      matrix.(List.assoc v1 !personnal_clocks).(0) <- (Some (float_of_string v +. float_of_string v2),true);
		   ) !list_const_set
	 )
	 else if b1 && b2 then (
	   List.iter (fun matrix ->
		      matrix.(0).(List.assoc v !personnal_clocks) <- (Some (float_of_string v2 -. float_of_string v1),true);
		   ) !list_const_set
	 )
	 else if b then (
	  List.iter (fun matrix ->
		      matrix.(List.assoc v1 !personnal_clocks).(List.assoc v2 !personnal_clocks) <- (Some (float_of_string v),true);
		   ) !list_const_set
	 )
	 else if b1 then (
	   raise CannotResolveWithDBM
	 )
	 else if b2 then (
	   List.iter (fun matrix ->
		      matrix.(List.assoc v1 !personnal_clocks).(List.assoc v !personnal_clocks) <- (Some (float_of_string v2),true);
		   ) !list_const_set
	 )
	 else 
	   (
	     raise CannotResolveWithDBM
	   )
       else if String.equal "<=" op then
	 if b && b1 && b2 then
	   if (float_of_string v1) -. (float_of_string v2) > (float_of_string v) then raise CannotBePlayed
	   else ()
	 else if b && b1 then (
	   List.iter (fun matrix ->
		      matrix.(0).(List.assoc v2 !personnal_clocks) <- (Some (float_of_string v -. float_of_string v1),false);
		   ) !list_const_set
	 )
	 else if b && b2 then (
	   List.iter (fun matrix ->
		      matrix.(List.assoc v1 !personnal_clocks).(0) <- (Some (float_of_string v +. float_of_string v2),false);
		   ) !list_const_set
	 )
	 else if b1 && b2 then (
	   List.iter (fun matrix ->
		      matrix.(0).(List.assoc v !personnal_clocks) <- (Some (float_of_string v2 -. float_of_string v1),false);
		   ) !list_const_set
	 )
	 else if b then (
	  List.iter (fun matrix ->
		      matrix.(List.assoc v1 !personnal_clocks).(List.assoc v2 !personnal_clocks) <- (Some (float_of_string v),false);
		   ) !list_const_set
	 )
	 else if b1 then (
	   raise CannotResolveWithDBM
	 )
	 else if b2 then (
	   List.iter (fun matrix ->
		      matrix.(List.assoc v1 !personnal_clocks).(List.assoc v !personnal_clocks) <- (Some (float_of_string v2),false);
		   ) !list_const_set
	 )
	 else 
	   (
	     raise CannotResolveWithDBM
	   )
       else failwith "not supported operator"
		     
    | _ -> failwith "not supported expression"
  with
    Not_found -> failwith "free time variable"

		  
let rec make_constraints_set trace test frame =
  try
  (*Printf.printf "\n\n trace = %s \n current variable = %d\n" (show_trace trace) !current_variable; print_matrix (List.nth !list_const_set 0);*)
  match trace, test with
  | (_, Fun("world", [Fun("!begin!", []); ir])) -> make_constraints_set trace ir frame
  | (_, Fun("check_run", tr)) ->  make_constraints_set trace (Fun("world",(Fun("!begin!", []))::tr)) frame
  | (NullTrace, Fun("empty", [])) -> (*!list_const_set*) ()
  | (Trace(Input(id, z, x), pr), Fun("world", [Fun("!in!", [r]); ir])) ->
     current_variable := !current_variable +1;
     if not (String.equal "!none!" z) then personnal_clocks := (z,!current_variable) :: !personnal_clocks;
     add_elapsing_time ();
     add_input_const id r;
     let new_pr = apply_subst_tr pr [x, apply_frame r frame] in
     make_constraints_set new_pr ir frame
			  
  | (Trace(Test(id,z,x, y), pr), Fun("world", [Fun("!test!", _); ir])) ->
     current_variable := !current_variable +1;
     if not (String.equal "!none!" z) then personnal_clocks := (z,!current_variable) :: !personnal_clocks;
     add_elapsing_time ();
     make_constraints_set pr ir frame
			  
  | (Trace(Output(id, z, x), pr), Fun("world", [Fun("!out!", _); ir])) ->
     current_variable := !current_variable +1;
     if not (String.equal "!none!" z) then personnal_clocks := (z,!current_variable) :: !personnal_clocks;
     add_elapsing_time ();
     outputs := (!nb_outputs, (id, !global_time))::(!outputs);
     nb_outputs := 1 + !nb_outputs;
     make_constraints_set pr ir (List.append frame [x])
			  
  | (Trace(TestTime(id,z,op, x, y), pr), Fun("world", [Fun("!testTime!", _); ir])) ->
     current_variable := !current_variable +1;
     if not (String.equal "!none!" z) then personnal_clocks := (z,!current_variable) :: !personnal_clocks;
     add_elapsing_time ();
     add_timing_constraint op x y;
     make_constraints_set pr ir frame
			  
  | (Trace(Let(id,z,x, t), pr), Fun("world", [Fun("!let!", _); ir])) ->
     current_variable := !current_variable +1;
     if not (String.equal "!none!" z) then personnal_clocks := (z,!current_variable) :: !personnal_clocks;
     add_elapsing_time ();
     begin
       match R.normalize t Theory.rewrite_rules with
       | Fun (s, []) as s' when startswith s "!n!" -> make_constraints_set (apply_subst_tr pr [x,s']) ir frame
       | Fun (s, []) as s'->
	  begin
	    match float_of_string_opt s with
	    | None -> failwith "2"
	    | Some v ->  make_constraints_set (apply_subst_tr pr [x,s']) ir frame;
	  end
       | VarTime _ as s' -> make_constraints_set (apply_subst_tr pr [x,s']) ir frame;
       | _ -> raise CannotBePlayed
     end 	       
  | _ ->
     Printf.printf "\n\n Trace = %s \n Test = %s \n\n" (show_trace trace) (show_term test);
     failwith "make_constraints_set: not compatible trace and test"
  with Not_found -> failwith "Not_found in make_constraint_set"
	      
let min_m (m1, b1) (m2, b2) =
  match m1, m2 with
  | Inf, _ -> (m2,b2)
  | _, Inf -> (m1,b1)
  | Some v1, Some v2 ->
     if v1 = v2 then (m1,b1 || b2)
     else if v1 < v2 then (m1,b1)
     else (m2,b2)

let add_m (m1, b1) (m2, b2) =
  match m1 with
  | Inf -> (Inf, b1)
  | Some v1 ->
     begin
       match m2 with
       | Inf -> (Inf, b2)
       | Some v2 -> (Some (v1 +. v2), b1 || b2)
     end

let floyd_warshall matrix =
  let n = Array.length matrix in
  for k = 0 to n - 1 do
    for i = 0 to n - 1 do
      for j = 0 to n - 1 do
	let mij = matrix.(i).(j) in
	let mik = matrix.(i).(k) in
	let mkj = matrix.(k).(j) in
	let update = min_m mij (add_m mik mkj) in
	matrix.(i).(j) <- update
      done;
    done;
  done

    	      
exception Attack of trace * term
exception Sat
exception NotSat

let check_satisfiability matrix =
  let n = Array.length matrix in
  for i = 0 to n - 1 do
    let v,b = matrix.(i).(i) in
    match v,b with
    | Some v, false when v < 0. -> raise NotSat
    | Some v, true when v <= 0. -> raise NotSat
    | _ -> ()
  done;
  raise Sat
	
let verify_constraints trace tests =
  (*check_init_clocks ();*)
  try 
    List.iter (
	fun x ->
	try 
	  (*counter := Util.add 1 !counter;
	    Printf.printf "Tests : %d/%d" !counter nb_tot; *)
	  global_time := 1;
	  personnal_clocks := [];
	  nb_outputs := 0;
	  outputs := [];
	  let n = (trace_size trace) + 2 in (* plus 1 for the constant zero and plus 1 for the initial time *)
	  (*Printf.printf "\n matrix size = %d\n" n;*)
	  let init_matrix = Array.make_matrix n n (Inf, false) in
	  current_variable := 1;
	  init_matrix.(0).(!current_variable) <- (Some (0. -. (List.assoc "init" !init_clock_values)), false);
	  init_matrix.(!current_variable).(0) <- (Some (List.assoc "init" !init_clock_values), false);
	  list_const_set := [init_matrix];
	  (*Printf.printf "\n\n"; print_matrix (List.nth !list_const_set 0);*)
	  make_constraints_set trace x []; 
	  (*Printf.printf "\n test = %s" (show_term x);*)
	  List.iter (fun m ->
		     try
		       floyd_warshall m;
		       check_satisfiability m
		     with
		     | Sat -> raise (Attack (trace,x))
		     | NotSat -> ()
		    ) !list_const_set
	with
	| CannotBePlayed -> ()
	| CannotResolveWithDBM -> Printf.printf "simplexe call\n"; TimeConst.verify_constraints_simplexe trace [x]
      ) tests
  with
  | Not_found -> failwith "Not_found in verify_constraints"
  | Failure "float_of_string" -> Printf.printf "\ncannot be verified using DBMs\n"; exit 0
	     
	
	  
let test () =
  let array = Array.make_matrix 3 3 (Some 0., false) in
  print_matrix array
