// ----------------------------------------- *)
//           Hancke & Kuhn PROTOCOL          *)
// ----------------------------------------- *)

// -- Protocol Scheme -- *)
// P -> V: nP
// V -> P: nV
// V -> P: b
// P -> V: Answer(b,KDF1(kVP,nP,nV),KDF2(kVP,nP,nV))

symbols pair/2, fst/1, snd/1, answer/3, kdf1/3, kdf2/3;

private end, 
		kVP, kA1P, kVA1, kA2P, kVA2,
		nV, b, nV2, b2, nV3, b3, nVO, bO,
		nP, nP2, nP3, nPO;

var x,y, 
	xNp, xAns, xNp2, xAns2, xNp3, xAns3,
	xNv, xB, xNv2, xB2, xNv3, xB3;
	
time_var z1, z2, z3, z4, z5, z6;
	
rewrite fst(pair(x,y)) -> x;
rewrite snd(pair(x,y)) -> y;

// Topology for Mafia frauds
topology = [v0,(0,0,0),hon], [p0,(2,0,0),hon], [a1,(0,0,0),dis], [a2,(2,0,0),dis]; 

V0 = (v0, in(xNp)).
	(v0, out(nV)).
	(v0,z1,out(b)).
	(v0,z2,in(xAns)).
	(v0, [|z2-z1 < 4|]).
	(v0,[ xAns = answer(b, kdf1(kVP,xNp,nV), kdf2(kVP,xNp,nV)) ]).
	(v0,out(end)).
	0;

P0 = (p0, out(nP)).
	(p0, in(xNv)).
	(p0,in(xB)).
	(p0, out( answer(xB, kdf1(kVP,nP,xNv), kdf2(kVP,nP,xNv)) )).
	0;

V2 = (v0, in(xNp2)).
	(v0, out(nV2)).
	(v0,z3,out(b2)).
	(v0,z4,in(xAns2)).
	(v0, [|z4-z3 < 4|]).
	(v0,[ xAns2 = answer(b2, kdf1(kVA1,xNp2,nV2), kdf2(kVA1,xNp2,nV2)) ]).
	0;

V3 = (v0, in(xNp3)).
	(v0, out(nV3)).
	(v0,z5,out(b3)).
	(v0,z6,in(xAns3)).
	(v0, [|z6-z5 < 4|]).
	(v0,[ xAns3 = answer(b3, kdf1(kA1P,xNp3,nV3), kdf2(kA1P,xNp3,nV3)) ]).
	0;

P2 = (p0, out(nP2)).
	(p0, in(xNv2)).
	(p0,in(xB2)).
	(p0, out( answer(xB2, kdf1(kA1P,nP2,xNv2), kdf2(kA1P,nP2,xNv2)) )).
	0;

P3 = (p0, out(nP3)).
	(p0, in(xNv3)).
	(p0,in(xB3)).
	(p0, out( answer(xB3, kdf1(kVP,nP3,xNv3), kdf2(kVP,nP3,xNv3)) )).
	0;

Pinit1 = (a1,out(kA1P)).(a1,out(kVA1)).0;
Pinit2 = (a2,out(kA2P)).(a2,out(kVA2)).0;
Poracle = (v0,out(kdf1(kVP,nPO,nVO))).(v0,out(kdf2(kVP,nPO,nVO))).(v0,out(bO)).(v0,out(nPO)).(v0,out(nVO)).(v0,out(answer(bO, kdf1(kVP,nPO,nVO), kdf2(kVP,nPO,nVO))));

Pprox = 
	Pinit1 :: Pinit2 :: Poracle :: 
    (
	V0 || P0 ||
	P2 ||
	V2 ||
	//P3 ||
	//V3 ||
	0
    );
	
initTime = 0;

is_reach end in Pprox ;
