# Symbolic verification of distance-bounding protocol considering a bounded number of sessions

This folder contains the requested material to perform the analyses presented in Chapter 3 related to the new procedure built over the [Akiss tool](http://akiss.gforge.inria.fr).

### Akiss-DB

Akiss is a tool implementing a procedure proposed by Chadha et al. [1] enabling the verification of equivalence-based properties. It proceeds in two steps: first it computes a knowledge base which is a finite representation of all possible traces (including recipes) executable by the process under study. Relying on these knowledge bases it decides whether two processes are equivalent or not. This is the part we reuse in this work. 
However, we had to completely redesign the update function used during the computation of these knowledge bases to ensure the completeness of our procedure.

#### Installation

The sources of the modified version of Akiss implementing our procedure is available in the *src/* folder. 

In order to compile it, please run the *Makefile* that is provided.

**Note:** OCaml version 4.06 or higher is required.

#### How to use?

After the compilation an executable file is generated in the *src/* folder.

Execute "./akiss-db < your-protocol.api" to verify the protocol described in the *your-protocol.api* file.


#### Case studies

We applied our procedure to analyse a large number of distance bounding protocols and payment protocols. 
For each distance bounding protocol, we consider three distinct scenarios: one to verify mafia fraud resistance, one to verify terrorist fraud resistance, and 
one to verify distance hijacking resistance. 
For each of these scnerios, we consider the reduced topologies presented in Chapter 4. 

All the files are available in the corresponding folders. 

We recall in the following tables the results.
We indicate the number of roles (running in parallel) we consider and the number of traces (due to all the possible interleaving of the roles remaning after having performed POR optimisation) that have been analysed by the tool in order to conclude. 
Our algorithm stops as soon as an attack is found, and thus the number of possible interleavings is not relevant in this case.

<table>
   <tr>
       <th rowspan="2">Protocols</th>
       <th colspan="4">Mafia fraud</th>
       <td>&nbsp;</td>
       <th colspan="4">Terrorist fraud</th>
       <td>&nbsp;</td>
       <th colspan="4">Distance hijacking</th>
       <!--<td>&nbsp;</td>
       <th>Protocols</th>
       <th>MF</th>
       <th>DH</th>-->
   </tr>
   <tr>
		<th>#roles</th>
		<th>#traces</th>
		<th>time</th>
		<th>status</th>
		<td>&nbsp;</td>
		<th>#roles</th>
		<th>#traces</th>
		<th>time</th>
		<th>status</th>
		<td>&nbsp;</td>
		<th>#roles</th>
		<th>#traces</th>
		<th>time</th>
		<th>status</th>
		
   </tr>
   <tr>
       <td>TREAD-Asymmetric &nbsp;&nbsp;</td>
       <td align=center> 2 </td>
       <td align=center> - </td>
       <td align=center> 1s </td>
       <td align=center> ❌ </td>
       <td>&nbsp;</td>
       <td align=center> ? </td>
       <td align=center> ? </td>
       <td align=center> ? </td>
       <td align=center> ? </td>
       <td>&nbsp;</td>
       <td align=center> 2 </td>
       <td align=center> - </td>
       <td align=center> 1s </td>
       <td align=center> ❌ </td>
   </tr>
   <tr>
       <td>SPADE </td>
       <td align=center> 2 </td>
       <td align=center> - </td>
       <td align=center> 2s </td>
       <td align=center> ❌ </td>
       <td>&nbsp;</td>
       <td align=center> 2 </td>
       <td align=center> - </td>
       <td align=center> 3s </td>
       <td align=center> ✅ </td>
       <td>&nbsp;</td>
       <td align=center> 2 </td>
       <td align=center> - </td>
       <td align=center> 4s </td>
       <td align=center> ❌ </td>
   </tr>
   <tr>
       <td>TREAD-Symmetric </td>
       <td align=center> 4 </td>
       <td align=center> 7500 </td>
       <td align=center> 18min </td>
       <td align=center> ✅ </td>
       <td>&nbsp;</td>
       <td align=center> ? </td>
       <td align=center> ? </td>
       <td align=center> ? </td>
       <td align=center> ? </td>
       <td>&nbsp;</td>
       <td align=center> 2 </td>
       <td align=center> - </td>
       <td align=center> 1s </td>
       <td align=center> ❌ </td>
   </tr>
   <tr>
       <td>Brands and Chaum</td>
       <td align=center> 4 </td>
       <td align=center> 5635 </td>
       <td align=center> 37min </td>
       <td align=center> ✅ </td>
       <td>&nbsp;</td>
       <td align=center> <i> o.o.s. </i> </td>
       <td align=center> <i> o.o.s. </i> </td>
       <td align=center> <i> o.o.s. </i> </td>
       <td align=center> <i> o.o.s. </i> </td>
       <td>&nbsp;</td>
       <td align=center> 2 </td>
       <td align=center> - </td>
       <td align=center> 1s </td>
       <td align=center> ❌ </td>
   </tr>
   <tr>
       <td>Swiss-Knife</td>
       <td align=center> 3 </td>
       <td align=center> 1080 </td>
       <td align=center> 25s </td>
       <td align=center> ✅ </td>
       <td>&nbsp;</td>
       <td align=center> ? </td>
       <td align=center> ? </td>
       <td align=center> ? </td>
       <td align=center> ? </td>
       <td>&nbsp;</td>
       <td align=center> 3 </td>
       <td align=center> 7470 </td>
       <td align=center> 4min </td>
       <td align=center> ✅ </td>
   </tr>
   <tr>
       <td rowspan="3">Hancke and Kuhn</td>
       <td align=center> 3 </td>
       <td align=center> 20 </td>
       <td align=center> 1s </td>
       <td align=center> ✅ </td>
       <td>&nbsp;</td>
       <td align=center> 3 </td>
       <td align=center> 20 </td>
       <td align=center> 1s </td>
       <td align=center> ❌ </td>
       <td>&nbsp;</td>
       <td align=center> 3 </td>
       <td align=center> 20 </td>
       <td align=center> 1s </td>
       <td align=center> ✅ </td>
   </tr>
   <tr>
	   <td align=center> 4 </td>
	   <td align=center> 3360 </td>
       <td align=center> 58s </td>
       <td align=center> ✅ </td>
       <td>&nbsp;</td>
       <td align=center> 4 </td>
       <td align=center> 3360 </td>
       <td align=center> 65s </td>
       <td align=center> ❌ </td>
       <td>&nbsp;</td>
       <td align=center> 4 </td>
	   <td align=center> 3360 </td>
       <td align=center> 47s </td>
       <td align=center> ✅ </td>
   </tr>
   <tr>
	   <td align=center> 5 </td>
	   <td align=center> 30240 </td>
       <td align=center> 14min </td>
       <td align=center> ✅  </td>
       <td>&nbsp;</td>
       <td align=center> 5 </td>
       <td align=center> 30240 </td>
       <td align=center> 25min </td>
       <td align=center> ❌ </td>
       <td>&nbsp;</td>
       <td align=center> 5 </td>
	   <td align=center> 30240 </td>
       <td align=center> 12min </td>
       <td align=center> ✅  </td>
   </tr>      
   <tr>
       <td>NXP</td>
       <td align=center> 2 </td>
       <td align=center> 126 </td>
       <td align=center> 4s </td>
       <td align=center> ✅ </td>
       <td>&nbsp;</td>
       <td align=center> ? </td>
       <td align=center> ? </td>
       <td align=center> ? </td>
       <td align=center> ? </td>
       <td>&nbsp;</td>
       <td align=center> 2 </td>
       <td align=center> - </td>
       <td align=center> 1s </td>
       <td align=center> ❌ </td>
   </tr>
   <tr>
       <td>PaySafe</td>
       <td align=center> 2 </td>
       <td align=center> 4 </td>
       <td align=center> 5min </td>
       <td align=center> ✅ </td>
       <td>&nbsp;</td>
       <td align=center> ? </td>
       <td align=center> ? </td>
       <td align=center> ? </td>
       <td align=center> ? </td>
       <td>&nbsp;</td>
       <td align=center> 2 </td>
       <td align=center> - </td>
       <td align=center> 3s </td>
       <td align=center> ❌ </td>
   </tr>
   <tr>
       <td>MasterCard-RRP</td>
       <td align=center> 2 </td>
       <td align=center> 36 </td>
       <td align=center> 6min </td>
       <td align=center> ✅ </td>
       <td>&nbsp;</td>
       <td align=center> ? </td>
       <td align=center> ? </td>
       <td align=center> ? </td>
       <td align=center> ? </td>
       <td>&nbsp;</td>
       <td align=center> 2 </td>
       <td align=center> - </td>
       <td align=center> 8s </td>
       <td align=center> ❌ </td>
   </tr>
   
   <caption>Results on our case studies of distance bounding protocols <br> (❌: attack found,  ✅: proved  secure, <i> o.o.s. </i>: out of scope) </caption>
</table>



[1] R. Chadha, V. Cheval, S. Ciobaca, and S. Kremer. *Automated verification of equivalence properties of cryptographic protocol*. ACM Transactions on Computational Logic, 2016. 